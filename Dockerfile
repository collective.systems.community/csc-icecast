# Copyright (C) 2020 collective-systems
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

FROM alpine:3.11

RUN apk update

# tzdata is for installing zdump in order to view time settings of /etc/localtime inside the container
# python and pip is necessary for supervisord
RUN apk add \
      bash \
      tzdata \
      icecast \
      nano \
      wget \
      curl \
      gettext \
      xmlstarlet \
      netcat-openbsd \
      python2 \
      py2-pip

RUN rm /var/cache/apk/*

RUN pip install supervisor supervisor-stdout

RUN mkdir /etc/icecast /dump /app /tools /templates

RUN chown -R icecast:icecast /dump
RUN chown -R icecast:icecast /etc/icecast

# /etc/icecast is the path for a volume that contains a icecast.xml file
#   if that file is supposed to be used, set ICECAST_CFG_FILE_SOURCE=default
# /var/log/icecast contains icecast logs
# /dump contains the stream recordings, if enabled via ICECAST_ENABLE_MOUNT_DUMP=1
# /etc/localtime is mounted to host's /etc/localtime for using time settings from host
VOLUME ["/etc/icecast" "/var/log/icecast" "/dump" "/etc/localtime"]

COPY templates /templates

# we add a small custom mime.types file as alpine does not have one
# and icecast complains about it missing on startup
COPY etc/mime.types /etc/mime.types
COPY src/envvars-defauls.cfg /app/envvars-defauls.cfg

COPY tools/generate-icecast-cfg.sh /tools/generate-icecast-cfg.sh
RUN chmod +x /tools/generate-icecast-cfg.sh

COPY tools/envsubst.sh /tools/envsubst.sh
RUN chmod +x /tools/envsubst.sh

COPY src/run-icecast.sh /app/run-icecast.sh
RUN chmod +x /app/run-icecast.sh

EXPOSE 8000
ENTRYPOINT ["/bin/bash", "/app/run-icecast.sh"]
