#!/usr/bin/env bash

# Copyright (C) 2020 collective-systems
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# set -x

SCRIPT_DIR=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
cd "${SCRIPT_DIR}"

declare -r __version="1.0.0"

declare -r __templates_dir="../templates"
declare -r __templates_ext="tmpl"
declare -r __templates_prefix="icecast"

declare -A __icecast_cfg_templates=(
  ["main"]=1
  ["limits"]=1
  ["authentication"]=1
  ["directory"]=0
  ["listen-socket"]=1
  ["http-headers"]=1
  ["relay"]=0
  ["mount-default"]=0
  ["mount"]=0
  ["paths"]=1
  ["logging"]=1
  ["security"]=1
)

declare -r __icecast_cfg_templates_order=(
  "main"
  "limits"
  "authentication"
  "directory"
  "listen-socket"
  "http-headers"
  "relay"
  "mount-default"
  "mount"
  "paths"
  "logging"
  "security"
)

declare -A __icecast_cfg_enable_mount_sub_sections=(
  ["stream"]=0
  ["dumpfile"]=0
)

declare -A __iceast_enable_xml_section_map=(
  ["ENV_ICECAST_XML_ENABLE_DIRECTORY"]="directory"
  ["ENV_ICECAST_XML_ENABLE_DEFAULT_MOUNT"]="mount-default"
  ["ENV_ICECAST_XML_ENABLE_RELAY"]="relay"
)

# store begin and end envvar in each entry
declare -A __iceast_enable_sub_section_env_map=(
  ["ENV_ICECAST_XML_ENABLE_MOUNT_DUMP"]="ENV_ICECAST_XML_BEGIN_MOUNT_DUMP ENV_ICECAST_XML_END_MOUNT_DUMP"
  ["ENV_ICECAST_XML_ENABLE_MOUNT_STREAM_SETTINGS"]="ENV_ICECAST_XML_BEGIN_MOUNT_STREAM_SETTINGS ENV_ICECAST_XML_END_MOUNT_STREAM_SETTINGS"
)

# per default, subsection are disabled by xml-comment tags
# in order to enable subsctions, xml-comment-tags must be removed/set to empty string
declare -A __icecast_enable_envvars=(
  ["ENV_ICECAST_XML_BEGIN_MOUNT_DUMP"]='<!--'
  ["ENV_ICECAST_XML_END_MOUNT_DUMP"]='-->'
  ["ENV_ICECAST_XML_BEGIN_MOUNT_STREAM_SETTINGS"]='<!--'
  ["ENV_ICECAST_XML_END_MOUNT_STREAM_SETTINGS"]='-->'
)

print-help-short() {
  printf '%s\n' "
usage:

  generate-icecast-cfg.sh h|-h|--help
  generate-icecast-cfg.sh hs|--hs|--help-short
  generate-icecast-cfg.sh he|--he|--help-examples
  generate-icecast-cfg.sh --version

  generate-icecast-cfg.sh e|envvars

  generate-icecast-cfg.sh
"
}

print-help-details() {
  printf '%s\n' "
description:

  this script generates a icecast-configuration in xml
    format. all values in xml-output are taken from
    particulr environment variables.

output:

  generated icecast xml-config is written to stdout and can
    be piped to a file or another program.

envvars:

  all necessary envvars with must have been set before
    invoking this script. if certain envvars are not set,
    the corresponding xml-tags in the xml output will be
    without value.

  envar names have to start with prefix ENV_ICECAST_XML.
    for a list of envvars, run:

    generate-icecast-cfg.sh envvars

  each envvar corresponds to a particular value of a icecast
    xml-config file. envvar names match a particular xml-section
    and tag-name.

  a set of envvars whose names start with ENV_ICECAST_XML_ENABLE_<xml-section>
    can be used to enable/disable the corresponding <xml-section>
    in the output xml-config.

  if ENV_ICECAST_XML_ENABLE_<xml-section> is set to 1, the
    corresponding <xml-section> will be available in the xml-
    output. if set to 0, the corresponding <xml-section> won't
    be available in the xml-output.

  for a description of icecast config values, please refer to the
    offical icecast documentation at:

    https://icecast.org/docs/icecast-2.4.1/
"
}

print-help-examples() {
  printf '%s\n' "
examples:

  # for a quick test, run without setting any envvars
  generate-icecast-cfg.sh

  # set envvars to enable/disable main sections in xml output
  # but no config values
  export ENV_ICECAST_XML_ENABLE_RELAY=1
  export ENV_ICECAST_XML_ENABLE_DIRECTORY=0
  export ENV_ICECAST_XML_ENABLE_DEFAULT_MOUNT=0
  generate-icecast-cfg.sh

  # set envvars to enable/disable subsections in xml output
  # but no config values
  export ENV_ICECAST_XML_ENABLE_DEFAULT_MOUNT=1
  export ENV_ICECAST_XML_ENABLE_MOUNT_DUMP=1
  export ENV_ICECAST_XML_ENABLE_MOUNT_STREAM_SETTINGS=1
  generate-icecast-cfg.sh

  # now, set all envvars corresponding to config values and
  # enable/disable particular sections
  export ENV_ICECAST_XML_ENABLE...
  export ENV_ICECAST_XML...
  generate-icecast-cfg.sh
"
}

print-version() {
  printf '%s\n' "${__version}"
}

print-envvars() {
  local envvarNames=()
  local configNames=()
  local enableNames=()
  local templatefile
  for key in "${__icecast_cfg_templates_order[@]}"; do
    tmplFile="${__templates_dir}/${__templates_prefix}-${key}.${__templates_ext}"
    [[ ! -f "${tmplFile}" ]] && continue
    envvarNames+=( $(cat "${tmplFile}" | grep -o -E 'ENV_ICECAST_XML_[A-Z0-9\_]+' ) )
  done

  for n in "${envvarNames[@]}"; do
    [[ ! "${n}" =~ ^ENV_ICECAST_XML_(BEGIN|END) ]] && configNames+=("${n}")
  done

  printf 'enable/disable xml-sections:\n'
  printf '%s\n' "${!__iceast_enable_xml_section_map[@]}"
  printf '\n'
  printf 'enable/disable xml-subsections:\n'
  printf '%s\n' "${!__iceast_enable_sub_section_env_map[@]}"
  printf '\n'
  printf 'xml-config values:\n'
  printf '%s\n' "${configNames[@]}"
}

populate-sub-section-envvars() {
  for e in "${!__icecast_enable_envvars[@]}"; do
    printf -v "${e}" '%s' "${__icecast_enable_envvars[${e}]}"
    export "${e}"
  done
}

select-xml-sections() {
  local sectionName
  for e in "${!__iceast_enable_xml_section_map[@]}"; do

    [[ -z "${!e}" ]] && continue
    [[ ! "${!e}" =~ ^(0|1)$ ]] && {
      printf 'error: invalid value [%s] in envvar [%s] ... skipping\n' "${!e}" "${e}"
      continue
    }
    sectionName="${__iceast_enable_xml_section_map[${e}]}"
    __icecast_cfg_templates["${sectionName}"]="${!e}"
  done
}

select-xml-sub-sections() {

  local beginEndEnvvars=()
  local envvarBegin
  local envvarEnd
  for e in "${!__iceast_enable_sub_section_env_map[@]}"; do

    [[ -z "${!e}" ]] && continue
    [[ ! "${!e}" =~ ^(0|1)$ ]] && {
      printf 'error: invalid value [%s] in envvar [%s] ... skipping\n' "${!e}" "${e}"
      continue
    }
    # do not quote, as we want word splitting in order to get
    # begin and end envvar name for corresponding subsection
    beginEndEnvvars=( ${__iceast_enable_sub_section_env_map[${e}]} )

    [[ ${!e} -eq 1 ]] && {
      # begin
      __icecast_enable_envvars[${beginEndEnvvars[0]}]=""
      # end section
      __icecast_enable_envvars[${beginEndEnvvars[1]}]=""
    }
  done
}

wrap-xml() {
  printf '<icecast>\n\n'
  cat -
  printf '</icecast>\n'
}

add-license() {
  local -r licenseFile="${__templates_dir}/${__templates_prefix}-license.${__templates_ext}"
  cat "${licenseFile}"
  printf '\n'
  cat -
}

select-templates() {

  # 1. compile all cfg snippets in order to generate the icecast template file
  # 2. substitute all envars to generate the icecst.xml config file
  local isEnabled
  local tmplFile
  local xmlOut=()
  for key in "${__icecast_cfg_templates_order[@]}"; do

    isEnabled="${__icecast_cfg_templates[${key}]}"
    [[ ${isEnabled} -eq 0 ]] && continue

    tmplFile="${__templates_dir}/${__templates_prefix}-${key}.${__templates_ext}"
    cat "${tmplFile}"
    printf '\n'
  done
}

do-generate() {

  case "$1" in
    hs|--hs|--help-short)
      print-help-short
      return 0
      ;;
    h|-h|--help)
      print-help-short
      print-help-details
      return 0
      ;;
    he|--he|--help-examples)
      print-help-examples
      return 0
      ;;
    --version)
      print-version
      return 0
      ;;
    e|envvars)
      print-envvars
      return 0
      ;;
    *) ;;
  esac

  select-xml-sections
  select-xml-sub-sections
  populate-sub-section-envvars

  select-templates | ./envsubst.sh "ENV_ICECAST_XML" | wrap-xml | add-license
}

do-generate "$@"
