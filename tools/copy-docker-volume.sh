#!/usr/bin/env bash

# Copyright (C) 2020 collective-systems
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# set -x

declare -r __version='1.3.0'

declare -r __host_copy_path="./volume-copies"

print-version() {
  printf '%s\n' "${__version}"
}

print-help-short() {
  printf '%s\n' "
usage:

  copy-docker-volumes.sh h|-h|--help
  copy-docker-volumes.sh hs|--hs|--help-short
  copy-docker-volumes.sh he|--he|--help-examples

  copy-docker-volumes.sh --version

  copy-docker-volumes.sh [-d|--dry-run]
                         <docker-volume-uri>
                         <local-directory>

  copy-docker-volumes.sh [-d|--dry-run]
                         <docker-volume-uri> ... <docker-volume-uri>
                         <local-directory>

  copy-docker-volumes.sh ls|list-volumes
                         <docker-volume-uri> ... <docker-volume-uri>
"
}

print-help-details() {
  printf '%s\n' "
description

  copies all data from one or more docker volumes of
    particular docker containers on remote hosts.

docker volume uris - overview:

  # copies a particular volume from a docker service
  <server-name>/<docker-service>/<volume>

  # copies all volumes from a docker service
  <server-name>/<docker-service>

  # copies volumes according to regex patterns

    # select several volumes
    <server-name>/<docker-service>/(<volume>|<volume>|<volume>)

    # select all volumes
    <server-name>/<docker-service>/*

    # select from pattern
    <server-name>/<docker-service>/<prefix>-[0-9]+

docker volume uris - hostname

  the host name to copy volume data from to local machine.

  it is expected that the selected docker service is running
    on that host.

  the host name can be provided in various flavours:

    a name referenced in ~/.ssh/config

      if a ssh key is defined in local ssh-configuration,
        a password prompt will be displayed to enter the
        ssh-key password on each ssh/scp connection attempt.

      the same happens, if only password authentication has been
        defined in the hosts ssh-configuration. then the password
        pompts are for entering the host's user password.

      the host user should be defined in the ssh host configuration.
        if not available, the current user of the local machine will
        be used instead but will most likely result in a connection
        error, except when user name on remote host is the same as
        the local user.

      if you pass the ssh-key/host-user password via stdin to this
        script. no prompts will be displayed.

    a url or ip-address

      password authentication must be enabled for a particular host-user,
        as ssh/scp skips host definitions in ~/.ssh/config. that means
        that it is (currently) not possible to select a local ssh-key
        for authentication on remote host.

      you can add a username in front of the host-address in order
        to authenticate with a particular user on remote host:

        ie. myuser@220.342.6.7

docker volume uris - docker service name

  the name of a running docker container to copy volume data from.

  the container name is that name that is set when starting a docker
    service via 'docker run --name container-name' or defined in
    docker-compose file as service name.

docker volume uris - docker volume name

  the name of a docker volume that is mounted into the given docker
    container.

  a volume name can be provided in various ways:

    - the volume name can be left empty. then all volumes of the
      given container will be copied

    - the volume name can be a pattern expression (as supported
      by bash without activating extended regex). then all volumes
      are copied that match this expression

    - the volume name can be a single name, then only that particular
      volume gets copied

  the referenced volume(s) should be named volume(s) that has been
    created before starting a container. named volume are usually
    mounted via the '-v' flag of 'docker run' and are created via
    'docker volume create'.

  when using the '--volume-from' flag in 'docker run', the given volume
    name should correspond to the volume of the from-container.

  anonymous volumes are more difficult to reference, as their name
    is randomly generated and can only be figured out via 'docker inspect'.
    anonymous volumes are created automatically if no named-volume
    is specified in 'docker run'.

stdin input:

  the password for the host ssh-key can be passed via stdin.
    the password is either a ssh-key password or the password
    of a user on the remote host.

  if no password is provided via stdin, a password prompt will
    be displayed on each ssh and scp connection attempt.

options:

  -d|--dry-run

    do not actually copy files but just show what happens.

  ls|list-volumes)

    print all volumes for any given volume-uri.
"
}

print-help-examples() {
  printf '%s\n' "
examples:

  # use a name defined in ssh config, and pass ssh-key password stored
  # in a local variable, via stdin
  read -r -s mypwd
  copy-icecast-volumes my-host/my-service/my-volume ./dump <<<\"\${mypwd}\"

  # copy all volumes containing 'my' at any position in their names
  copy-icecast-volumes my-host/icecast-server/my ./dump <<<\"\${mypwd}\"

  # copy all volumes starting with 'my' in their names
  copy-icecast-volumes my-host/icecast-server/^my ./dump <<<\"\${mypwd}\"

  # copy all volumes ending with 'volume' in their names
  copy-icecast-volumes my-host/icecast-server/volume$ ./dump <<<\"\${mypwd}\"

  # copy all volumes
  copy-icecast-volumes my-host/icecast-server/* ./dump <<<\"\${mypwd}\"
  copy-icecast-volumes my-host/icecast-server/.* ./dump <<<\"\${mypwd}\"
  copy-icecast-volumes my-host/icecast-server/ ./dump <<<\"\${mypwd}\"

  # use a host address with custom user, display password prompts
  copy-icecast-volumes myuser@mydomain.org/my-service/my-volume ./dump

  # use a host address with custom user, user password via stdin
  copy-icecast-volumes myuser@200.234.5.19/my-server/my-volume ./dump <<<\"\${mypwd}\"
"
}

do-copy() {

  local dryRun=0
  local listOnly=0

  local localDir
  local volumeUris

  while [ "$1" ]; do
    case "$1" in
      h|-h|--help)
        print-help-short
        print-help-details
        return 0
        ;;
      hs|--hs|--help-short)
        print-help-short
        return 0
        ;;
      he|--he|--help-examples)
        print-help-examples
        return 0
        ;;
      --version)
        print-version
        return 0
        ;;
      -d|--dry-run)
        dryRun=1
        ;;
      ls|list-volumes)
        listOnly=1
        ;;
      *)

        if [[ ${listOnly} -eq 1 ]]; then
          volumeUris=("$@")
        else
          local args=("$@")
          local numArgs="$#"
          local numVols=$((numArgs - 1 ))

          [[ ${numArgs} -lt 2 ]] && {
            printf 'error: you need to provide a local directory as copy destination ... stopping\n' >&2
            return 1
          }

          volumeUris=("${args[@]:0:${numVols}}")
          localDir="${args[@]:${numVols}}"

          [[ -z "${localDir}" ]] && {
            printf 'error: local directory name is empty ... stopping\n' >&2
            return 1
          }

          [[ ! -d "${localDir}" ]] && mkdir -p "${localDir}"
        fi

        break
        ;;
    esac

    shift
  done

  local hostNames=()
  local dockerServices=()
  local dockerVolumeRegexes=()

  local uriParts=()
  local volRegex
  for volUri in "${volumeUris[@]}"; do
    uriParts=($(cut -d '/' --output-delimiter ' ' -f 1,2 <<<"${volUri}"))

    hostNames+=("${uriParts[0]}")
    dockerServices+=("${uriParts[1]}")

    # remove host and docker container names from uri
    # the result should look like this

    # 1. my-host/my-container/my-volume
    #   => /my-container/my-volume
    #   => //my-volume
    #   => my-volume

    # 2. my-host/my-container/
    #   => /my-container/
    #   => //
    #   => <empty>

    # 3. my-host/my-container
    #   => /my-container
    #   => /
    #   => <empty>
    volRegex="${volUri/${uriParts[0]}/}"
    volRegex="${volRegex/${uriParts[1]}/}"

    # this detects cases 1 and 2
    if [[ "${volRegex}" =~ ^([/][/]|[/][/][^/]+)$ ]]; then
      dockerVolumeRegexes+=("${volRegex/\/\//}")
    # this detects case 3
    elif [[ "${volRegex}" =~ ^([/]|[/][^/]+)$ ]]; then
      dockerVolumeRegexes+=("${volRegex/\//}")
    else
      printf 'error: invalid uri [%s] ... stopping\n' "${volUri}" >&2
      return 1
    fi
  done

  [[ ${#hostNames[@]} -eq 0 ]] && {
    printf 'error: no host name provided. we need at least one host to copy stuff from .. stopping\n' >&2
    return 1
  }

  [[ ${#dockerServices[@]} -eq 0 ]] && {
    printf 'error: no docker container name provided. we need at least one docker container to copy stuff from .. stopping\n' >&2
    return 1
  }

  [[ ${#hostNames[@]} -ne ${#dockerServices[@]} ]] && {
    printf 'error: number of hosts and number of docker containers do not match .. stopping\n' >&2
    return 1
  }

  # read ssh key password from stdin, if available
  read -r -s -t 0.1 sshPwd

  # for each hostName, loop in background in order to run all volume copy commands in parallel
  local dockerInspectCmd
  local dockerCopyCmd

  local inspectJson
  local volumesJson
  local volumeJson

  local volumeNames=()
  local selectedVolumes=()
  local containerVolumePath
  local containerVolumeDir

  for i in "${!hostNames[@]}"; do

    hostName="${hostNames[$i]}"
    dockerContainer="${dockerServices[$i]}"

    dockerVolume="${dockerVolumeRegexes[$i]}"
    [[ -z "${dockerVolume}" ]] && dockerVolume='.*'
    [[ "${dockerVolume}" == '*' ]] && dockerVolume='.*'

    dockerInspectCmd="docker inspect \$(docker ps -f name=${dockerContainer} -q)"

    if [[ -z "${sshPwd}" ]]; then
      inspectJson=$(ssh "${hostName}" "${dockerInspectCmd}")
    else
      inspectJson=$(sshpass -P ass <<<"${sshPwd}" ssh "${hostName}" "${dockerInspectCmd}")
    fi

    volumesJson=$(jq '.[].Mounts[]|select(.Type == "volume")' <<<"${inspectJson}")
    volumeNames=($(jq -r '.Name' <<<"${volumesJson}"))

    # filter all volumes we want to copy by given volume name/regex
    for volName in "${volumeNames[@]}"; do
      [[ "${volName}" =~ ${dockerVolume} ]] && selectedVolumes+=("${volName}")
    done

    if [[ ${listOnly} -eq 1 ]]; then
      printf '%s\n' "${selectedVolumes[@]}"
      return 0
    fi

    # TODO copy in parallel, ie via a -p flag that defines the number of parallel file copies
    for volName in "${selectedVolumes[@]}"; do
      containerVolumePath=$(jq -r 'select(.Name == "'"${volName}"'").Destination' <<< "${volumesJson}")
      containerVolumeDir="$(basename ${containerVolumePath})"

      printf 'copy volume [%s:%s] from remote host [%s] to local host directory [%s]\n' "${volName}" "${containerVolumePath}" "${hostName}" "${localDir}"
      [[ ${dryRun} -eq 1 ]] && continue

      dockerCopyCmd="docker cp \$(docker ps -f name=${dockerContainer} -q):${containerVolumePath} ${__host_copy_path}"

      if [[ -z "${sshPwd}" ]]; then
        ssh  "${hostName}" "[[ ! -d ${__host_copy_path} ]] && mkdir -p ${__host_copy_path}; ${dockerCopyCmd}"
        scp -r "${hostName}:${__host_copy_path}/${containerVolumeDir}" "${localDir}"
      else
        sshpass -P ass <<<"${sshPwd}" ssh "${hostName}" "[[ ! -d ${__host_copy_path} ]] && mkdir -p ${__host_copy_path}; ${dockerCopyCmd}"
        sshpass -P ass <<<"${sshPwd}" scp -r "${hostName}:${__host_copy_path}/${containerVolumeDir}" "${localDir}"
      fi
    done

    if [[ -z "${sshPwd}" ]]; then
      ssh "${hostName}" "rm -r -f ${__host_copy_path}"
    else
      sshpass -P ass <<<"${sshPwd}" ssh "${hostName}" "rm -r -f ${__host_copy_path}"
    fi

  done
}

do-copy "$@"
