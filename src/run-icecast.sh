#!/usr/bin/env bash

  # Copyright (C) 2020 collective-systems
  #
  #   This program is free software: you can redistribute it and/or modify
  #   it under the terms of the GNU General Public License as published by
  #   the Free Software Foundation, either version 3 of the License, or
  #   (at your option) any later version.
  #
  #   This program is distributed in the hope that it will be useful,
  #   but WITHOUT ANY WARRANTY; without even the implied warranty of
  #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  #   GNU General Public License for more details.
  #
  #   You should have received a copy of the GNU General Public License
  #   along with this program.  If not, see <https://www.gnu.org/licenses/>.

# set -x

SCRIPT_DIR=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
cd "${SCRIPT_DIR}"

source ./envvars-defauls.cfg

declare -r __version="1.1.0"

declare -r __generate_icecast_cfg_bin="${SCRIPT_DIR}/../tools/generate-icecast-cfg.sh"

declare -r __supervisord_cfg_name="supervisord.conf"
declare -r __supervisord_template_file="${SCRIPT_DIR}/../templates/supervisord.template"

declare -r __retry_count_max=60
declare -r __retry_wait_sec=2

declare -A __icecast_cfg_files=(
  ["secrets"]=""
  ["default"]="icecast.xml"
  ["template"]="icecast-generic.xml"
)

declare -A __icecast_cfg_file_pathes=(
  ["secrets"]="/run/secrets"
  ["default"]="/etc/icecast"
  ["template"]="/etc/icecast"
)

# TODO split into main and internal array
# NOTE ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH is set to null as default pathes
#      are defined in __icecast_cfg_file_pathes
#
declare -r -A __main_envvars_defaults=(
  ["ENV_ICECAST_INTERNAL_BIN_NAME"]="${ENV_ICECAST_INTERNAL_BIN_NAME_DEFAULT}"
  ["ENV_ICECAST_INTERNAL_BIN_PATH"]="${ENV_ICECAST_INTERNAL_BIN_PATH_DEFAULT}"
  ["ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH"]=null
  ["ENV_ICECAST_INTERNAL_NETCAT_BIN_NAME"]="${ENV_ICECAST_INTERNAL_NETCAT_BIN_NAME_DEFAULT}"
  ["ENV_ICECAST_INTERNAL_NETCAT_BIN_PATH"]="${ENV_ICECAST_INTERNAL_NETCAT_BIN_PATH_DEFAULT}"
  ["ENV_ICECAST_INTERNAL_SUPERVISORD_BIN_NAME"]="${ENV_ICECAST_INTERNAL_SUPERVISORD_BIN_NAME_DEFAULT}"
  ["ENV_ICECAST_INTERNAL_SUPERVISORD_BIN_PATH"]="${ENV_ICECAST_INTERNAL_SUPERVISORD_BIN_PATH_DEFAULT}"
  ["ENV_ICECAST_INTERNAL_SUPERVISORD_CONFIG_FILE_PATH"]="${ENV_ICECAST_INTERNAL_SUPERVISORD_CONFIG_FILE_PATH_DEFAULT}"
  ["ENV_ICECAST_INTERNAL_SHRED_BIN_NAME"]="${ENV_ICECAST_INTERNAL_SHRED_BIN_NAME_DEFAULT}"
  ["ENV_ICECAST_INTERNAL_SHRED_BIN_PATH"]="${ENV_ICECAST_INTERNAL_SHRED_BIN_PATH_DEFAULT}"
  ["ENV_ICECAST_RUN_WITH_SUPERVISORD"]="${ENV_ICECAST_RUN_WITH_SUPERVISORD_DEFAULT}"
  ["ENV_ICECAST_CONFIG_FILE_SOURCE"]="${ENV_ICECAST_CONFIG_FILE_SOURCE_DEFAULT}"
  ["ENV_ICECAST_SECRETS_SHRED"]="${ENV_ICECAST_SECRETS_SHRED_DEFAULT}"
)

declare -A -r __icecast_xml_envvars_defaults=(
  ["ENV_ICECAST_XML_LOCATION"]="${ENV_ICECAST_XML_LOCATION_DEFAULT}"
  ["ENV_ICECAST_XML_ADMIN"]="${ENV_ICECAST_XML_ADMIN_DEFAULT}"
  ["ENV_ICECAST_XML_HOSTNAME"]="${ENV_ICECAST_XML_HOSTNAME_DEFAULT}"
  ["ENV_ICECAST_XML_LISTEN_SOCKET_PORT"]="${ENV_ICECAST_XML_LISTEN_SOCKET_PORT_DEFAULT}"
  ["ENV_ICECAST_XML_LIMITS_CLIENTS"]="${ENV_ICECAST_XML_LIMITS_CLIENTS_DEFAULT}"
  ["ENV_ICECAST_XML_LIMITS_SOURCES"]="${ENV_ICECAST_XML_LIMITS_SOURCES_DEFAULT}"
  ["ENV_ICECAST_XML_LIMITS_QUEUE_SIZE_BYTES"]="${ENV_ICECAST_XML_LIMITS_QUEUE_SIZE_BYTES_DEFAULT}"
  ["ENV_ICECAST_XML_LIMITS_BURST_ON_CONNECT"]="${ENV_ICECAST_XML_LIMITS_BURST_ON_CONNECT_DEFAULT}"
  ["ENV_ICECAST_XML_LIMITS_BURST_SIZE_BYTES"]="${ENV_ICECAST_XML_LIMITS_BURST_SIZE_BYTES_DEFAULT}"
  ["ENV_ICECAST_XML_AUTHENTICATION_SOURCE_PWD"]="${ENV_ICECAST_XML_AUTHENTICATION_SOURCE_PWD_DEFAULT}"
  ["ENV_ICECAST_XML_AUTHENTICATION_RELAY_PWD"]="${ENV_ICECAST_XML_AUTHENTICATION_RELAY_PWD_DEFAULT}"
  ["ENV_ICECAST_XML_AUTHENTICATION_ADMIN_PWD"]="${ENV_ICECAST_XML_AUTHENTICATION_ADMIN_PWD_DEFAULT}"
  ["ENV_ICECAST_XML_SECURITY_CHROOT"]="${ENV_ICECAST_XML_SECURITY_CHROOT_DEFAULT}"
  ["ENV_ICECAST_XML_LOGGING_LOG_LEVEL"]="${ENV_ICECAST_XML_LOGGING_LOG_LEVEL_DEFAULT}"
  ["ENV_ICECAST_XML_LOGGING_LOG_ARCHIVE"]="${ENV_ICECAST_XML_LOGGING_LOG_ARCHIVE_DEFAULT}"
  ["ENV_ICECAST_XML_LOGGING_LOG_SIZE_BYTES"]="${ENV_ICECAST_XML_LOGGING_LOG_SIZE_BYTES_DEFAULT}"
  ["ENV_ICECAST_XML_MOUNT_PUBLIC"]="${ENV_ICECAST_XML_MOUNT_PUBLIC_DEFAULT}"
  ["ENV_ICECAST_XML_MOUNT_HIDDEN"]="${ENV_ICECAST_XML_MOUNT_HIDDEN_DEFAULT}"
  ["ENV_ICECAST_XML_MOUNT_DUMP_FILE_NAME"]="${ENV_ICECAST_XML_MOUNT_DUMP_FILE_NAME_DEFAULT}"
  ["ENV_ICECAST_XML_MOUNT_STREAM_NAME"]="${ENV_ICECAST_XML_MOUNT_STREAM_NAME_DEFAULT}"
  ["ENV_ICECAST_XML_MOUNT_STREAM_DESCRIPTION"]="${ENV_ICECAST_XML_MOUNT_STREAM_DESCRIPTION_DEFAULT}"
  ["ENV_ICECAST_XML_MOUNT_STREAM_URL"]="${ENV_ICECAST_XML_MOUNT_STREAM_URL_DEFAULT}"
  ["ENV_ICECAST_XML_MOUNT_STREAM_GENRE"]="${ENV_ICECAST_XML_MOUNT_STREAM_GENRE_DEFAULT}"
  ["ENV_ICECAST_XML_RELAY_REMOTE_SERVER"]="${ENV_ICECAST_XML_RELAY_REMOTE_SERVER_DEFAULT}"
  ["ENV_ICECAST_XML_RELAY_REMOTE_PORT"]="${ENV_ICECAST_XML_RELAY_REMOTE_PORT_DEFAULT}"
  ["ENV_ICECAST_XML_RELAY_REMOTE_MOUNT"]="${ENV_ICECAST_XML_RELAY_REMOTE_MOUNT_DEFAULT}"
  ["ENV_ICECAST_XML_RELAY_LOCAL_MOUNT"]="${ENV_ICECAST_XML_RELAY_LOCAL_MOUNT_DEFAULT}"
  ["ENV_ICECAST_XML_RELAY_LOCAL_ON_DEMAND"]="${ENV_ICECAST_XML_RELAY_LOCAL_ON_DEMAND_DEFAULT}"
  ["ENV_ICECAST_XML_DIRECTORY_YP_URL"]="${ENV_ICECAST_XML_DIRECTORY_YP_URL_DEFAULT}"
  ["ENV_ICECAST_XML_DIRECTORY_YP_URL_TIMEOUT"]="${ENV_ICECAST_XML_DIRECTORY_YP_URL_TIMEOUT_DEFAULT}"
  ["ENV_ICECAST_XML_ENABLE_RELAY"]="${ENV_ICECAST_XML_ENABLE_RELAY_DEFAULT}"
  ["ENV_ICECAST_XML_ENABLE_MOUNT_DUMP"]="${ENV_ICECAST_XML_ENABLE_MOUNT_DUMP_DEFAULT}"
  ["ENV_ICECAST_XML_ENABLE_DEFAULT_MOUNT"]="${ENV_ICECAST_XML_ENABLE_DEFAULT_MOUNT_DEFAULT}"
  ["ENV_ICECAST_XML_ENABLE_DIRECTORY"]="${ENV_ICECAST_XML_ENABLE_DIRECTORY_DEFAULT}"
  ["ENV_ICECAST_XML_ENABLE_MOUNT_STREAM_SETTINGS"]="${ENV_ICECAST_XML_ENABLE_MOUNT_STREAM_SETTINGS_DEFAULT}"
)

print-help-short() {
  printf '%s\n' "
Usage:

  run-icecast.sh h|-h|--help
  run-icecast.sh hs|--hs|--help-short

  run-icecast.sh --version

  run-icecast.sh [--dry-run] [-v|--verbose]

  run-icecast.sh s|show c|current
  run-icecast.sh s|show s|supported
  run-icecast.sh s|show d|defaults

  run-icecast.sh t|status p|pathes
  run-icecast.sh t|status s|secrets
  run-icecast.sh t|status b|binaries
"
}

print-help-details() {
    printf '%s\n' "
Run options:

  --dry-run (optional)

    Dry-run allows to observe if the internal container run-script
      generates a proper icecast config-file or picks up a proper
      config-file from a mount volume or Rancher secret volume.

    The icecast server is not started and the container will just exit.
      Use in conjunction with --verbose.

  -v|--verbose (optional)

    Enables more logging:

    - shows the selected icecast config-file content
    - shows the supervisord config-file content
        (if supervisord has been enabled)
    - shows the retry count while detecting if icecast server is running
      (if secrets and secrets removal has been enabled)

Help and version commands:

  h|-h|--help

    Show detailed help description and exit.

  hs|--hs|--help-short

    Show a short version of the help desciption and exit.

  v|version|--version

    Show script version and exit.

Show commands:

  s|show c|current

    Show a list of environment variables that have been set on
      container start.

    Environment variables are listed if they start with the prefix
      'ENV_ICECAST'. All other environment variables won't be shown.

    If no envvars have been set, the output of this command is empty.

    If environment variables have been set, the output also contains
      a 'supported' or 'not supported' hint.

  s|show s|supported

    Show a list of ALL environment variable names that are supported
      by the container.

    All supported environment variable names begin with the prefix
      'ENV_ICECAST'.

  s|show d|defaults

    Show a list of default values for ALL environment variables that
      are supported by the container.

    Default values are used if the corresponding environment variable
      has not been set on container start.

Status commands:

  t|status p|pathes

    Show a list of icecast config file pathes used by the script.

    This script selects the proper path according to the value set
      via ENV_ICECAST_CONFIG_FILE_SOURCE or its default.

    If path is tweaked via envvar ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH,
      this change is reflected in the list.

  t|status b|binaries

    Show a list of internally used binaries and their configured pathes.

    This command is useful if internal envvars have been set
      to tweak binary pathes, ie. for testing.

  t|status s|secrets

    Show a list of secrets related infos.

    This command is useful to show the status of a mounted secrets file.
      ENV_ICECAST_CONFIG_FILE_SOURCE=secrets must have been set to run
      this command.

    The secret's status info will also show how a secret has been mounted
      (as read-only or writable), in order to figure out if the secret
      file can be deleted once it has been consumed by the icecast server
      (by setting envvar ENV_ICECAST_SECRETS_SHRED=1).

Main environment variables:

  The following environment variable can be set to configure main
    runtime behaviour of the container:

    ENV_ICECAST_CONFIG_FILE_SOURCE

      Tells the run-script from where to obtain the icecast config-file.

      Accepted values:

        [${__icecast_cfg_files[*]}]

      Default value:

        [${__main_envvars_defaults[ENV_ICECAST_CONFIG_FILE_SOURCE]}]

    ENV_ICECAST_RUN_WITH_SUPERVISORD

      Enables or disables the use of supervisord to run the icecast-server
        inside the container. supervisord will automatically restart
        the icecast-server once it sguts down or crashes.

      Accepted values:

        [0 1]

      Default value:

        [${__main_envvars_defaults[ENV_ICECAST_RUN_WITH_SUPERVISORD]}]

    ENV_ICECAST_SECRETS_SHRED

      Enables or disables the deletion of the icecast config, when
        mounted as secret.

      The secret configuration file can only be deleted if

        - supervisord is disabled via ENV_ICECAST_RUN_WITH_SUPERVISORD=0.
            otherwise, the secrets config-file won't be found anymore
            once supervisord restarts icecast after a crash.
        - ENV_ICECAST_CONFIG_FILE_SOURCE=secrets, in order to enable the
            usage of the a secret config-file
        - if Rancher secrets are used

      NOTE: this enables an experimental feature and will most likely be
        changed or removed in the future.

      NOTE: Enable secrets deletion with care. The only use-case where
          secrets deletion inside the docker container works properly,
          is when Rancher secrets are used and are mounted as Read-Write
          into the container!

        Docker secrets cannot be deleted as they are mounted on a Read-Only
          tmpfs into the container by default. Thus, even if the secrets file
          have write permission, the Read-Only tmpfs it resides in prevents
          file alteration.

        Secrets removal via the corresponding docker command

            docker service update --secret-rm my-icecast-secret ...

          won't work either in case of the icecast container, as the container
          does not persists the icecast configuration to survive a container
          restart after an update.

          A docker service update would restart the container, thus, this
          run-script is invoked again, this time without mounted secrets. As
          the run-script is still configured via ENV_ICECAST_CONFIG_FILE_SOURCE
          to read the secrets file, it will complain about the missing secret
          now and exit.

      Accepted values:

        [0 1]

      Default value:

        [${ENV_ICECAST_SECRETS_SHRED_DEFAULT}]

Environment variables for generating a icecast config-file

  If 'ENV_ICECAST_CONFIG_FILE_SOURCE' is set to 'template', the
    values of environment variables that begin with prefix
    'ENV_ICECAST_XML' are taken into account when generating the
    config-file on container start.

  For a list of corresponding environment variables, run the container
    with the 'show supported' command.

Internal environment variables

  NOTE: tweaking internal environment variables is usually not
    necessary and should be done with care.

  A couple of interal-use-only environment variables can be tweaked
    to change the behaviour of the run-script.

  The corresponding environment variable begin with prefix
    'ENV_ICECAST_INTERNAL'.

  The main reasons to change those variables are:

    - Unit testing on a local machine without docker container
        environment.

      For some tests it is necessary to mock the icecast or
        supervisord binaries in order to avoid starting the
        server during testing.

    - Using this script as standalone script without docker
        container environment.

      Depending on the Linux distribution, the installed icecast
        binary is sometimes called 'icecast2' instead of 'icecast'.
        If the icecast-binary is installed on a custom location,
        the path to the binary may also diverge from the usual
        installation path.
"
}

# writes the value of a given environment or global variable to stdout
#
# @param $1 - the name of the environment or global variable to read
# @return 0 - if a environment or global variable exists for the given name,
#               its value is printed to stdout.
#             if the variable exists but is empty,
#               nothing is printed to stdout
#             if the variable does not exists,
#               nothing is printed to stdout
# @return 1 - the provided envvar name argument is empty.
#               nothing is printed to stdout.
get-value-from-envvar() {
  local -r envvarName="$1"
  [[ -z "${envvarName}" ]] && return 1
  printf '%s' "${!envvarName}"
  return 0
}

# writes the value of a given environment or global variable to stdout
#   or a default value if variable does not exist
#
# @param $1 - the name of the environment or global variable to read
# @param $2 - a default value that is printed to stdout if given environment
#               variable is empty or does not exist.
# @return 0 - if a environment or global variable exists for the given name,
#               its value is printed to stdout.
#             if variable exists but is empty,
#               default value is printed to stdout
#             if variable does not exist,
#               default value is printed to stdout
# @return 1 - the provided envvar name argument is empty.
#               nothing is printed to stdout.
#             the provided default value is empty.
#               nothing is printed to stdout.
get-value-from-envvar-or-default() {
  local -r envvarName="$1"
  local -r defaultVal="$2"

  [[ -z "${defaultVal}" ]] && return 1

  local envvarVal
  envvarVal=$(get-value-from-envvar "$1")
  [[ $? -ne 0 ]] && return 1

  [[ -z "${envvarVal}" ]] && envvarVal="${defaultVal}"
  printf '%s' "${envvarVal}"
  return 0
}

# validates that the given file is a properly constructed xml-file
#
# @param $1 - the full path of the file to validate
# @return 0 - given file is a proper xml file
# @return 1 - given file is no xml file or xml syntax errors have been detected
validate-xml-file() {
  local -r xmlFile="$1"

  xmlstarlet val "${xmlFile}" >/dev/null
  return $?
}

get-mount-options() {
  local -r path="$1"

  # first we try to find the given given path in the mount list
  local mountDevice=( $(grep -w "${path}" /proc/mounts) )

  # if there was no match, we try to figure out the mount device for the given path
  if [[ "${#mountDevice[*]}" -eq 0 ]]; then
    local mountPath
    # NOTE: we cannot use df's --output option, as this option is not available
    #       in docker container that are using busybox. busybox df has a more
    #       limited set of options
    #
    # the 6th column of df output contains the mount device the path resides in
    read _ _ _ _ _ mountPath < <(df "${path}" | tail -1)

    # now lets search for that mount device in the mount list
    mountDevice=( $(grep -w "${mountPath}" /proc/mounts ) )

    [[ "${#mountDevice[*]}" -eq 0 ]] && {
      printf 'error: could detect general mount options of mountpoint [%s] ... stopping\n' "${mountPath}" >&2
      return 1
    }
  fi

  # mountDevice is a list that contains all infos for the mounted device,
  # as provided by /proc/mounts
  #
  # the 4th element contains the mount options
  # ie.: tmpfs /run/secrets tmpfs rw,relatime,size=10240k 0 0
  local -r opts="${mountDevice[3]}"
  local -r optsList=( ${opts//,/ } )

  printf '%s\n' "${optsList[@]}"
}

is-mounted-as-writable() {
  local -r path="$1"
  local -r optsList=( $(get-mount-options "${path}") )

  # here we try to match the 'ro' or 'rw' mount options
  local o
  for o in "${optsList[@]}"; do
    [[ "${o}" =~ ^(rw|ro)$ ]] && break
  done

  if [[ "${o}" == 'rw' ]]; then
    return 0
  elif [[ "${o}" == 'ro' ]]; then
    return 1
  else
    printf 'error: could not detect read-write options in mount options [%s] for mount device [%s] ... stopping\n' "${optsList[*]}" "${path}" >&2
    return 1
  fi
}

generate-generic-icecast-cfg-file() {

  local value
  local placeholder
  for envvarName in "${!__icecast_xml_envvars_defaults[@]}"; do

    value=$(get-value-from-envvar ${envvarName})
    [[ $? -ne 0 ]] && print-envvar-error "${envvarName}" >&2 && return 1

    # set envvar from default values
    [[ -z "${value}" ]] && {
      printf -v "${envvarName}" '%s' "${__icecast_xml_envvars_defaults[${envvarName}]}"
      export "${envvarName}"
    }
  done

  ${__generate_icecast_cfg_bin}
}

# generates a supervisord config-file from a given temnplate and writes
#   the file content to stdout.
#
# the template file must have defined a placeholder for value substitution.
#   that placeholder should comply to the format expected by 'envsubst'.
#
# the placeholder will be substituted by the command to run the icecast
#   server.
#
# the placeholder should be specified as ${ENV_SUPERVISORD_ICECAST_COMMAND}
#   in the template file
#
# @param $1 - the full path of a supervisord config file template
# @param $2 - the icecast command to be run by supervisord. that command
#             (including all arguments) should be passed as single string
#             and will substitute the corresponding placeholder in the
#             template file
generate-supervisord-cfg-file() {
  local -r templateFile="$1"
  local -r icecastCmd="$2"

  printf -v ENV_SUPERVISORD_ICECAST_COMMAND '%s' "${icecastCmd}"
  export ENV_SUPERVISORD_ICECAST_COMMAND

  envsubst <"${templateFile}"
}

# generates the full path of a program on the local filesystem (local machine, docker container)
#   from two environment variables that hold the program's path and program's name
#
# if specified environment variables are set, their values will be used for full path construction
# if specified environment variables are not set, the corresponding default values defined in
#   __main_envvars_defaults will be used instead.
#
# @param $1 - the NAME of the environment variable that holds the path of a binary on the local fs
# @param $2 - the NAME of the environment variable that holds the name of a binary in the path on the local fs
# @return 0 - prints full path of binary to stdout
# @return 1 - envvar arguments for binary path and/or binary name are empty strings.
#             no full path could have been constructed, nothing is printd to sdout
generate-binary-file-path-from-envvars() {
  local -r envvarBinPath="$1"
  local -r envvarBinName="$2"

  local binName
  binName=$(get-value-from-envvar-or-default "${envvarBinName}" "${__main_envvars_defaults[${envvarBinName}]}")
  [[ $? -ne 0 ]] && print-envvar-error "${envvarBinName}" >&2 && return 1

  local binPath
  binPath=$(get-value-from-envvar-or-default "${envvarBinPath}" "${__main_envvars_defaults[${envvarBinPath}]}")
  [[ $? -ne 0 ]] && print-envvar-error "${envvarBinPath}" >&2 && return 1

  # TODO remove trailing slash in path, if any
  local -r binFullPath="${binPath}/${binName}"
  [[ ! -f "${binFullPath}" ]] && print-file-not-found-error "${binFullPath}" >&2 && return 1

  printf '%s' "${binFullPath}"
}

# prints the full path of the icecast configuration file, as selected
#   by the config-file source, to stdout
#
# for proper config-file path construction, init-cfg-file-path() and
#   init-cfg-file-name() should have been called beforehand.
#
# @param $1 - the source of the config file. value can be any key existing in
#             __icecast_cfg_file_pathes.
# @return 0 - the full config-file path is written to stdout
# @return 1 - the path and/or name of the config-file is empty
#             (in __icecast_cfg_file_pathes and/or __icecast_cfg_files)
#             no path could be constructed and nothing is printed to stdout
generate-icecast-cfg-file-path() {
  local -r icecastCfgFileSource="$1"

  local -r icecastCfgFilePath="${__icecast_cfg_file_pathes[${icecastCfgFileSource}]}"
  local -r icecastCfgFile="${__icecast_cfg_files[${icecastCfgFileSource}]}"

  [[ -z "${icecastCfgFilePath}" ]] || [[ -z "${icecastCfgFile}" ]] && return 1

  printf '%s/%s' "${icecastCfgFilePath}" "${icecastCfgFile}"
}

# prints the full path of the supervisord configuration file to stdout
#
# the path of the supervisord config-file is either taken from
#
#   - environment variable ENV_ICECAST_INTERNAL_SUPERVISORD_CONFIG_FILE_PATH
#     if set
#   - or the default value for ENV_ICECAST_INTERNAL_SUPERVISORD_CONFIG_FILE_PATH,
#     if not set
#
# the name of of the supervisord config file is fixed and taken from global
#   variable __supervisord_cfg_name
#
# @return 0 - the full config-file path is written to stdout
# @return 1 - the path and/or name of the config-file is empty
#             (in ENV_ICECAST_INTERNAL_SUPERVISORD_CONFIG_FILE_PATH or its default,
#             and/or in __supervisord_cfg_name)
#             no path could be constructed and nothing is printed to stdout
generate-supervisord-cfg-file-path() {
  local supervisordCfgPath
  supervisordCfgPath=$(get-value-from-envvar-or-default ENV_ICECAST_INTERNAL_SUPERVISORD_CONFIG_FILE_PATH "${__main_envvars_defaults[ENV_ICECAST_INTERNAL_SUPERVISORD_CONFIG_FILE_PATH]}")
  [[ $? -ne 0 ]] && print-envvar-error "ENV_ICECAST_INTERNAL_SUPERVISORD_CONFIG_FILE_PATH" >&2 && return 1

  [[ -z "${supervisordCfgPath}" ]] || [[ -z "${__supervisord_cfg_name}" ]] && return 1

  printf '%s/%s' "${supervisordCfgPath}" "${__supervisord_cfg_name}"
}

print-cfg-file() {
  local -r cfgFile="$1"
  [[ ! -f "${icecastCfgFile}" ]] && return 1

  printf '%s\n' "-----------------------------------"
  cat "${cfgFile}"
  printf '%s\n\n' "-----------------------------------"
  return 0
}

print-envvar-error() {
  [[ -z "${1}" ]] && return 0
  printf 'error: could not obtain value from envvar [%s] or its default ... stopping\n' "${1}"
  return 0
}

print-file-not-found-error() {
  [[ -z "${1}" ]] && return 0
  printf 'error: file not found at [%s] ... stopping\n' "${1}"
  return 0
}

print-generation-from-template-error() {
  [[ -z "${1}" ]] && return 0
  printf 'error: generation of config file for [%s] from template failed ... stopping\n' "${1}" >&2
  return 0
}

# prints all EXTERNALLY SET environment variable name and their values as key=value pair to stdout
#   variables are set BEFORE script invokation by the caller of this script
#   variables are clusterd and printed line by line in the following order
#
# - main variables: all EXTERNALLY SET variables whose names start with ENV_ICECAST but do not contain XML or INTERNAL
# - xml config file variables: all EXTERNALLY SET variables whose names start with ENV_ICECAST_XML
# - internal variables: all EXTERNALLY SET variables whose names start with ENV_ICECAST_INTERNAL
print-current-envvars() {
  local isSupported
  local envvarName
  while read -t 0.1 -r envvarNameValue; do
    isSupported="supported"
    envvarName="${envvarNameValue%=*}"
    [[ -z "${__main_envvars_defaults[${envvarName}]}" ]] && isSupported="not supported"
    printf '%s [%s]\n' "${envvarNameValue}" "${isSupported}"
  done < <(env | grep -i "ENV_ICECAST" | grep -v -E "(XML|INTERNAL)")

  while read -t 0.1 -r envvarNameValue; do
    isSupported="supported"
    envvarName="${envvarNameValue%=*}"
    [[ -z "${__icecast_xml_envvars_defaults[${envvarName}]}" ]] && isSupported="not supported"
    printf '%s [%s]\n' "${envvarNameValue}" "${isSupported}"
  done < <(env | grep -i "ENV_ICECAST_XML")

  while read -t 0.1 -r envvarNameValue; do
    isSupported="supported"
    envvarName="${envvarNameValue%=*}"
    [[ -z "${__main_envvars_defaults[${envvarName}]}" ]] && isSupported="not supported"
    printf '%s [%s]\n' "${envvarNameValue}" "${isSupported}"
  done < <(env | grep -i "ENV_ICECAST_INTERNAL")
}

# prints all DEFINED environment variable names, that can be set by the caller of this script
#   supported variables names are DEFINED as keys in __main_envvars_defaults and  __icecast_xml_envvars_defaults
#   variables are clusterd and printed to stdout in the following order
#
# - main variables: all DEFINED variables whose names start with ENV_ICECAST but do not contain XML or INTERNAL
# - internal variables: all DEFINED variables whose names start with ENV_ICECAST_INTERNAL
# - xml config file variables: all DEFINED variables whose names start with ENV_ICECAST_XML
print-supported-envvars() {
  printf '%s\n' "${!__main_envvars_defaults[@]}" | sort
  printf '%s\n' "${!__icecast_xml_envvars_defaults[@]}" | sort
}

# prints all INTERNALLY SET default environment variable name and their default values as key=value pair to stdout
#   default variables are INTERNALLY SET on script invokation via 'source ./envvars-defauls.cfg' (see first lines of this script)
#   variables are clusterd and printed line by line in the following order
#
# - main variables: all INTERNALLY SET variables whose names start with ENV_ICECAST but do not contain XML or INTERNAL
# - xml config file variables: all INTERNALLY SET variables whose names start with ENV_ICECAST_XML
# - internal variables: all INTERNALLY SET variables whose names start with ENV_ICECAST_INTERNAL
print-default-envvars() {
  local key
  local value
  while read -t 0.1 -r kv; do
    key="${kv%=*}"
    key="${key%_*}"
    value="${kv##*=}"
    printf '%s=%s\n' "${key}" "${value}"
  done < <(set -o posix; set | grep -i "ENV_ICECAST.*DEFAULT=" | grep -v -E "(XML|INTERNAL)")

  while read -t 0.1 -r kv; do
    key="${kv%=*}"
    key="${key%_*}"
    value="${kv##*=}"
    printf '%s=%s\n' "${key}" "${value}"
  done < <(set -o posix; set | grep -i "ENV_ICECAST_XML.*DEFAULT=")

  while read -t 0.1 -r kv; do
    key="${kv%=*}"
    key="${key%_*}"
    value="${kv##*=}"
    printf '%s=%s\n' "${key}" "${value}"
  done < <(set -o posix; set | grep -i "ENV_ICECAST_INTERNAL.*DEFAULT=")
}

print-cfg-file-pathes() {
  local value
  for key in "${!__icecast_cfg_file_pathes[@]}"; do
    value="${__icecast_cfg_file_pathes[${key}]}"
    printf '%s=%s\n' "${key}" "${value}"
  done
}

print-secrets-status() {
  local -r secretsMountPath="${__icecast_cfg_file_pathes[secrets]}"
  local -r secretsFilePath="${secretsMountPath}/${__icecast_cfg_files[secrets]}"

  local -r secretsFilePerm=$(ls -l -1 "${secretsFilePath}")
  local -r secretsMountOpts="$(get-mount-options "${secretsFilePath}")"

  local secretsFileRemovable=0
  is-mounted-as-writable "${secretsFilePath}"
  [[ $? -eq 0 ]] && secretsFileRemovable=1

  printf '%s\n' "
secrets file path       : ${secretsFilePath}
secrets file permission : ${secretsFilePerm}
secrets file removable  : ${secretsFileRemovable}
secrets mount options   : ${secretsMountOpts}
"
}

print-binary-status() {
  printf '%s\n' "
icecast    : $1
supervisord: ${2:-not used}
netcat     : ${3:-not used}
shred      : ${4:-not used}
"
}

print-version() {
  printf '%s\n' "${__version}"
}

# inits the the config-file name for the provided config-file-source.
#
# if source == "secrets", the icecast config-file name will be taken
#   from any file that is mounted on the secrets location, as that
#   secrets file can be named arbitrarily. the detected file name is
#   stored in __icecast_cfg_files.
#
#   the secrets location is taken from __icecast_cfg_file_pathes map.
#   __icecast_cfg_file_pathes must have been properly initialised already
#   via init-cfg-file-path()
#
# if source != "secrets", __icecast_cfg_files wont be updated, as it
#   provides proper file names already.
#
# @param $1 - the source of the config file. value can be any key existing in
#             __icecast_cfg_file_pathes.
# @return 0 - if source == secrets: a single secrets file has been detected at the
#             secrets mount point its name is written to __icecast_cfg_files
#             if source != secrets:  __icecast_cfg_files does not need to be updated
# @return 1 - if source == secrets: more than on secrets file has been found at the
#             secrets mount point
init-cfg-file-name() {
  local -r icecastCfgFileSource="$1"

  [[ "${icecastCfgFileSource}" != "secrets" ]] && return 0

  local icecastCfgFilePath="${__icecast_cfg_file_pathes[${icecastCfgFileSource}]}"

  [[ ! -d "${icecastCfgFilePath}" ]] && {
    printf 'error: secrets mount point expected at [%s] does not exist ... stopping\n' "${icecastCfgFilePath}" >&2
    return 1
  }

  # we only expect one file mounted at the secrets path
  local secrets
  secrets=( $(find "${icecastCfgFilePath}" -type f -exec basename {} \;) )

  [[ ${#secrets[*]} -ne 1 ]] && {
    printf 'error: expected one secrets-file at [%s] but got [%s] ... stopping\n' "${icecastCfgFilePath}" "${secrets[*]}" >&2
    return 1
  }

  __icecast_cfg_files["${icecastCfgFileSource}"]="${secrets[0]}"
}

# inits the config-file path for the provided config-file-source if
#   environment variable ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH has
#   been set
#
# if set, the envvar value will be written to __icecast_cfg_file_pathes
#   for the current config-file source.
#
# if not set, the path for the given source defined in __icecast_cfg_file_pathes
#   remains as is
#
# @param $1 - the source of the config file. value can be any key existing
#             in __icecast_cfg_file_pathes
init-cfg-file-path() {
  local -r icecastCfgFileSource="$1"

  local icecastCfgFilePathFromEnv
  icecastCfgFilePathFromEnv=$(get-value-from-envvar ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH)
  [[ $? -ne 0 ]] && print-envvar-error "ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH" >&2 && return 1

  # only update __icecast_cfg_file_pathes if environment variable has been set and is not empty
  # otherwise, the existing path for the given source remains as is
  [[ ! -z "${icecastCfgFilePathFromEnv}" ]] && {
    # TODO remove trailing slash in path, if any
    __icecast_cfg_file_pathes["${icecastCfgFileSource}"]="${icecastCfgFilePathFromEnv}"
  }
  return 0
}

run-icecast-default() {
  local -r icecastBin="$1"
  local -r icecastCfgFile="$2"

  ${icecastBin} -c "${icecastCfgFile}"
}

run-icecast-supervisord() {
  local -r supervisordBin="$1"
  local -r supervisordCfgFile="$2"

  ${supervisordBin} -n -c "${supervisordCfgFile}"
}

run-icecast-secrets-removal() {
  local -r icecastBin="$1"
  local -r icecastCfgFile="$2"
  local -r onlyOverwrite="$3"
  local -r netcatBin="$4"
  local -r shredBin="$5"

  # figure out the server port
  # NOTE several port can be specified in an icecast config file
  local ports=()
  ports=($(xmlstarlet sel -t -v /icecast/listen-socket/port "${icecastCfgFile}"))

  # use the default port if no port has been specified in config file
  local port="${__icecast_xml_envvars_defaults[ENV_ICECAST_XML_LISTEN_SOCKET_PORT]}"
  # if more than one port has been specified, we just use the first one
  [[ "${#ports[*]}" -gt 0 ]] && [[ "${ports[0]}" =~ ^[0-9]+$ ]] && port="${ports[0]}"

  # start icecast in background
  local pid
  ${icecastBin} -c "${icecastCfgFile}" &
  pid=$!

  # wait until icecast is available
  # we try for roughly two minutes to reach the port
  local retryCount=1
  local isRunning=0
  until [ ${retryCount} -eq ${__retry_count_max} ]; do
    # in first loop run, the initial sleep will give the icecast server some time
    # to start. sleeping here also quites the loop immediately if retryCount has
    # reached its max value.
    sleep ${__retry_wait_sec}
    [[ "${logLevel}" == "verbose" ]] && printf 'debug: probe port [%s] ... %s\%s\n' "${port}" "${retryCount}" "${__retry_count_max}" >&2

    ${netcatBin} -w 2 localhost "${port}"
    [[ $? -eq 0 ]] && isRunning=1 && break

    retryCount=$((retryCount+1))
  done

  [[ ${isRunning} -eq 0 ]] && {
    printf 'error: cannot not delete secrets file as icecast took too long to start ... stopping\n' >&2
    return 1
  }

  is-mounted-as-writable "${icecastCfgFile}"
  [[ $? -eq 1 ]] && {
    printf 'error: cannot not delete secrets file as it is mounted read-only ... stopping\n' >&2
    return 1
  }
  local shredOpts=(-f -z -n 7)
  [[ ${onlyOverwrite} -eq 0 ]] && shredOpts+=(-u)

  ${shredBin} "${shredOpts[@]}" "${icecastCfgFile}"
  [[ $? -ne 0 ]] && {
    printf 'error: could not delete secrets file [%s] ... stopping\n' "${icecastCfgFile}" >&2
    kill "${pid}"
    return 1
  }

  # wait until process dies
  wait "${pid}"
  res=$?
}

do-run() {

  local logLevel="info"
  local cmd="run"
  local subCmd

  while [ "$1" ]; do
    case "$1" in
      h|-h|--help)
        print-help-short
        print-help-details
        return 0
        ;;
      hs|--hs|--help-short)
        print-help-short
        return 0
        ;;
      --version)
        print-version
        return 0
        ;;
      --dry-run)
        cmd=dryrun
        ;;
      -v|--verbose)
        logLevel="verbose"
        ;;
      s|show)
        shift
        cmd=show
        subCmd="$1"
        ;;
      t|status)
        shift
        cmd=status
        subCmd="$1"
        ;;
      *)
        printf 'error: unknown cl argument [%s] ... stopping\n' "$1" >&2
        return 1
        ;;
    esac
    shift
  done

  if [[ "${cmd}" == "show" ]]; then
    case "${subCmd}" in
      c|current)
        print-current-envvars
        return 0
        ;;
      s|supported)
        print-supported-envvars
        return 0
        ;;
      d|defaults)
        print-default-envvars
        return 0
        ;;
      p|pathes)
        print-cfg-file-pathes
        return 0
        ;;
      *)
        printf 'error: unknown command [%s %s] ... stopping\n' "${cmd}" "${subCmd}" >&2
        return 1
        ;;
    esac
  fi

  local icecastCfgFileSource
  icecastCfgFileSource=$(get-value-from-envvar ENV_ICECAST_CONFIG_FILE_SOURCE)
  [[ $? -ne 0 ]] && print-envvar-error "ENV_ICECAST_CONFIG_FILE_SOURCE" >&2 && return 1
  [[ -z "${icecastCfgFileSource}" ]] && icecastCfgFileSource="${__main_envvars_defaults[ENV_ICECAST_CONFIG_FILE_SOURCE]}"

  [[ -z "${__icecast_cfg_file_pathes[${icecastCfgFileSource}]}" ]] && {
    printf 'error: config file source value [%s] in envvar [%s] is not supported ... stopping\n' "${icecastCfgFileSource}" "ENV_ICECAST_CONFIG_FILE_SOURCE" >&2
    return 1
  }

  local runWithSupervisord
  runWithSupervisord=$(get-value-from-envvar ENV_ICECAST_RUN_WITH_SUPERVISORD)
  [[ $? -ne 0 ]] && print-envvar-error "ENV_ICECAST_RUN_WITH_SUPERVISORD" >&2 && return 1
  [[ -z "${runWithSupervisord}" ]] && runWithSupervisord="${__main_envvars_defaults[ENV_ICECAST_RUN_WITH_SUPERVISORD]}"

  local icecastShredSecrets
  icecastShredSecrets=$(get-value-from-envvar ENV_ICECAST_SECRETS_SHRED)
  [[ $? -ne 0 ]] && print-envvar-error "ENV_ICECAST_SECRETS_SHRED" >&2 && return 1
  [[ -z "${icecastShredSecrets}" ]] && icecastShredSecrets="${__main_envvars_defaults[ENV_ICECAST_SECRETS_SHRED]}"

  [[ ${icecastShredSecrets} -ge 1 ]] && [[ ${runWithSupervisord} -eq 1 ]] && {
    printf 'warning: secrets wont be deleted as supervisord is enabled\n' >&2
    icecastShredSecrets=0
  }

  [[ ${icecastShredSecrets} -ge 1 ]] && [[ "${icecastCfgFileSource}" != "secrets" ]] && {
    printf 'warning: secrets wont be deleted as config file is not configured as secrets [%s]\n' "${icecastCfgFileSource}" >&2
    icecastShredSecrets=0
  }

  init-cfg-file-path "${icecastCfgFileSource}"
  [[ $? -ne 0 ]] && return 1

  init-cfg-file-name "${icecastCfgFileSource}"
  [[ $? -ne 0 ]] && return 1

  local icecastCfgFile
  icecastCfgFile="$(generate-icecast-cfg-file-path ${icecastCfgFileSource})"
  [[ $? -ne 0 ]] && return 1

  local icecastBin
  icecastBin="$(generate-binary-file-path-from-envvars ENV_ICECAST_INTERNAL_BIN_PATH ENV_ICECAST_INTERNAL_BIN_NAME)"
  [[ $? -ne 0 ]] && return 1

  local supervisordBin
  [[ ${runWithSupervisord} -eq 1 ]] && {
    supervisordBin="$(generate-binary-file-path-from-envvars ENV_ICECAST_INTERNAL_SUPERVISORD_BIN_PATH ENV_ICECAST_INTERNAL_SUPERVISORD_BIN_NAME)"
    [[ $? -ne 0 ]] && return 1
  }

  local netcatBin
  local shredBin
  [[ ${icecastShredSecrets} -ge 1 ]] && {
    shredBin=$(generate-binary-file-path-from-envvars ENV_ICECAST_INTERNAL_SHRED_BIN_PATH ENV_ICECAST_INTERNAL_SHRED_BIN_NAME)
    [[ $? -ne 0 ]] && return 1
    netcatBin=$(generate-binary-file-path-from-envvars ENV_ICECAST_INTERNAL_NETCAT_BIN_PATH ENV_ICECAST_INTERNAL_NETCAT_BIN_NAME)
    [[ $? -ne 0 ]] && return 1
  }

  if [[ "${cmd}" == "status" ]]; then
    case "${subCmd}" in
      p|pathes)
        print-cfg-file-pathes
        return 0
        ;;
      s|secrets)
        if [[ "${icecastCfgFileSource}" != "secrets" ]]; then
          printf 'error: secrets status can only be shown if secrets usage is enabled via [ENV_ICECAST_CONFIG_FILE_SOURCE=secrets] ... \n' >&2
        else
          print-secrets-status
        fi
        return 0
        ;;
      b|binaries)
        print-binary-status "${icecastBin}" "${supervisordBin}" "${netcatBin}" "${shredBin}"
        return 0
        ;;
      *)
        printf 'error: unknown command [%s %s] ... stopping\n' "${cmd}" "${subCmd}" >&2
        return 1
        ;;
    esac
  fi

  [[ "${icecastCfgFileSource}" == "template" ]] && {
    printf 'info: icecast config file [%s] will be created from templates\n' "${icecastCfgFile}"

    generate-generic-icecast-cfg-file >"${icecastCfgFile}"
    [[ $? -ne 0 ]] && print-generation-from-template-error "icecast" >&2 && return 1
  }

  [[ ! -f "${icecastCfgFile}" ]] && print-file-not-found-error "${icecastCfgFile}" >&2 && return 1

  validate-xml-file "${icecastCfgFile}"
  [[ $? -ne 0 ]] && {
    printf 'error: provided file [%s] is no valid xml file ... stopping\n' "${icecastCfgFile}" >&2
    return 1
  }

  local supervisordCfgFile

  [[ ${runWithSupervisord} -eq 1 ]] && {
    supervisordCfgFile="$(generate-supervisord-cfg-file-path)"

    printf 'info: supervisord config file [%s] will be created from template\n' "${supervisordCfgFile}"
    # now we kow that a icecst config file is available
    # this is the icecast command to be run by supervisord
    local -r iceCmd="${icecastBin} -n -c ${icecastCfgFile}"

    generate-supervisord-cfg-file "${__supervisord_template_file}" "${iceCmd}" >"${supervisordCfgFile}"
    [[ $? -ne 0 ]] && print-generation-from-template-error "supervisord" >&2 && return 1

    [[ "${logLevel}" == "verbose" ]] && {
      printf '\ndebug: content of supervisord config file [%s]\n' "${supervisordCfgFile}"
      print-cfg-file "${supervisordCfgFile}"
    }
  }

  [[ "${logLevel}" == "verbose" ]] && {
    printf '\ndebug: content of icecast config file [%s]\n' "${icecastCfgFile}"
    print-cfg-file "${icecastCfgFile}"
  }

  [[ "${cmd}" == "dryrun" ]] && return 0

  if [[ ${runWithSupervisord} -eq 1 ]]; then
    subCmd="supervisord"
  elif [[ ${icecastShredSecrets} -ge 1 ]]; then
    subCmd="rmsecrets"
  else
    subCmd="default"
  fi

  local res
  case "${subCmd}" in
    supervisord)
      run-icecast-supervisord "${supervisordBin}" "${supervisordCfgFile}"
      res=$?
      ;;
    rmsecrets)
      local onlyOverwrite=0
      [[ ${icecastShredSecrets} -eq 2 ]] && onlyOverwrite=1
      run-icecast-secrets-removal  "${icecastBin}" "${icecastCfgFile}" "${onlyOverwrite}" "${netcatBin}" "${shredBin}"
      res=$?
      ;;
    default)
      run-icecast-default "${icecastBin}" "${icecastCfgFile}"
      res=$?
      ;;
    *)
      printf 'error: unknown command [%s %s] ... stopping\n' "${cmd}" "${subCmd}" >&2
      res=1
      ;;
  esac

  return ${res}
}

do-run "$@"
