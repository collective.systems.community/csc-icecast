[[_TOC_]]

# csc-icecast

Just another Icecast streaming server in a Docker container. 

This project particulary focus on easily deploying an `icecast-stack` in Rancher. The stack is currently consisting of a main icecast-server and a (optional) scalable number of relays. Deployment without Rancher is possible well. 

The container offers extensive possibilities to tweak the Icecast configuration via environment variables, besides using custom configuration files or secrets.

This Icecast container is based on the [offical Alpine Image](https://hub.docker.com/_/alpine/) (version 3.11) and contains [the official Icecast2 server](https://icecast.org) (version 2.4.4) from the corresponding alpine-package.

# Some Features

The icecast container can be deployed as standalone icecast-server or as relay that fetches and re-publishes a stream from another icecast-server or relay.

This repository contains `docker-compose.yml` and `rancher-compose.yml` files for deploying a icecast-server and one or more icecast-relays (an icecast-stack) on individual host-machines in Rancher (version 1.6). 

- For more information see the wiki-page about [deploying a icecast-stack with or without Rancher](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Deployment/Run-Icecast-Stack).

Similar to other icecast-containers, the configuration file for an icecast-server is generated and filled with data from particular environment variables on container start.

- [Environment variables are explained in more detail](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Environment/Brief-Introduction) in the wiki.

However, it is also possible to mount a custom config xml-file into the container or to make a config-file available via Rancher secrets (Docker secrets work as well). 

- For more information about config file generation and mounting, see the wiki page about [configuring a icecast container](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Deployment/Configure-Container).

The Icecast container provides a couple of status commands that may be used to help troubleshooting the usage of environment variables, as it is often quite difficult to easily detect errors in environment variables settings (wrong naming, wrong values, etc).

- For more information about invoking status commands, see the wiki page about [container commands](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Handbook/Container-Commands).

The generation of icecast configration files is decoupled from the container and might be used standalone without docker environment as well. A couple of unit tests are available for the corresponding scripts.

- For more information, see the corresponding wiki page about [container testing](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Handbook/Testing).  

# Quickstart

For all examples in the quickstart-guide, we assume that you are on a Linux machine (or let's say, all examples have been tested on a Debian based Linux distro). 

That machine can be your local machine, any remote machine or virtual machine with x86-64bit architecture. Maybe MacOS will also work. Arm-based machines (like a Raspberry-Pi) should in general be no problem at all, except that another alpine-base-image must be used for the particular arm-architecture. 

## Prebuild images

Prebuild images of `csc-icecast` are available for download.

##### Gitlab

The latest stable version that has been tested and is working:

- registry.gitlab.com/collective.systems.community/csc-icecast/csc-icecast:stable

The latest version that may be unstable or buggy:

- registry.gitlab.com/collective.systems.community/csc-icecast/csc-icecast

A list of all released containers can be found here:

- https://gitlab.com/collective.systems.community/csc-icecast/container_registry

##### Docker-Hub

`csc-icecast` still has to be added to docker-Hub.

## Build container image on local machine

``` bash
git clone https://gitlab.com/collective.systems.community/csc-icecast.git

cd csc-icecast

# uses the default Dockerfile from this repo
sudo docker build --rm -t iceast-server-image .
```

If you want to build the `csc-icecast` image on a ARM-based machine (like a Raspberry-Pi), you usually just need to replace the current Alpine-Image specified by the `FROM` directive in the `Dockerfile` of this repo, by the corresponding ARM-Alpine-Image. For the proper alpine-images, see the [supported architectures of alpine base-images](https://hub.docker.com/_/alpine?tab=description). 

## Deploy container as main icecast-server on local machine

In this example we use the local docker image `icecast-server-image` that has been built in the previous step.

The icecast config-file will be generated on container start, filled with [default values](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Environment/Brief-Introduction) only. 

In order to use only default values for config-file generation, none of the supported environment variables starting with `ENV_ICECAST_XML` should be set on container start. 

The icecast-container is configured as main streaming server, running on `localhost`, port `8000`, with `àdmin` and `source` passwords set to `hackme`.

``` bash
sudo docker run --rm -d --name my-icecast-server -p 8000:8000 icecast-server-image
```

Instead of using the locally built image `icecast-server-image` (see the  built step above), one can also specify a prebuilt image (those images will be downloaded first by `docker run`):

- gitlab: registry.gitlab.com/collective.systems.community/csc-icecast/csc-icecast:stable

``` bash
# or run prebuild image from gitlab
sudo docker run --rm -d --name my-icecast-server -p 8000:8000 registry.gitlab.com/collective.systems.community/csc-icecast/csc-icecast:stable
```

Afterwards, verify that the container is running:

```bash
# show running containers and filter by name containing 'icecast-server'
# output should at least show one running icecast-server container. copy
# its <container-id> from output.
sudo docker ps --filter name=icecast-server

# now check the logs of the container with the id retrieved from previous command
sudo docker logs <container-id>

# check if status page is reachable
curl localhost:8000
```

Now you can [start streaming and listening](#stream-to-local-icecast-server-and-visit-status-page).

## Deploy container as relay-server on local machine

In this example we configure the icecast container to run as relay. For that, a couple of [environment variables](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Environment/Brief-Introduction#relay-settings) must be set. 

The icecast config-file will be generated on container start. The config-file is filled with values of explicitly set environment variables and [default values](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Environment/Brief-Introduction) otherwise.

The icecast-container is configured as relay-server running on `localhost`, port `8001`, with `àdmin` and `source` passwords set to `hackme`. The icecast-server, that is to be relayed, is assumed to run on the same local machine on port `8000`. In order to reach the icecast-server, you need to figure out the ip address of your local machine, ie. by inspecting the output of `ifconfig` command. Here we assume that the local machine's ip address is `192.168.1.13`  

``` bash
sudo docker run --rm -d --name my-icecast-relay -p 8001:8000 \
                --env ENV_ICECAST_XML_ENABLE_RELAY=1 \
                --env ENV_ICECAST_XML_LIMITS_SOURCES=1 \
                --env ENV_ICECAST_XML_RELAY_REMOTE_SERVER=192.168.1.13 \
                --env ENV_ICECAST_XML_RELAY_REMOTE_PORT=8000 \
                --env ENV_ICECAST_XML_RELAY_REMOTE_MOUNT=/stream \
                --env ENV_ICECAST_XML_RELAY_LOCAL_MOUNT=/stream \
                icecast-server-image
```

Instead of using the locally built image `icecast-server-image` (see the  built step above), one can also specify a prebuilt image (those images will be downloaded first by `docker run`):

- gitlab: registry.gitlab.com/collective.systems.community/csc-icecast/csc-icecast:stable

Afterwards, verify that the container is running:

```bash
# show running containers and filter by name containing 'icecast-relay'
# output should at least show one running icecast-relay container. copy
# its <container-id> from output.
sudo docker ps --filter name=icecast-relay

# now check the logs of the container with the id retrieved from previous command
sudo docker logs <container-id>

# check if status page is reachable
curl localhost:8001
```

Now you can [start listening to the relay](#stream-to-local-icecast-server-and-visit-status-page).

## Deploy a Icecast-Stack on local machine

In the following examples, we run an `icecast-stack` on a local machine. The ip-address of the local machine is assumed to be `192.168.1.13`. Your address is most likely different and can be figured out with the `ifconfig` command.  

The stack consists of two containers: 

- main streaming-server, running on port `8000`
- relay-server that relays the main-server, running on port `8001`

**Note: Usually, it does not make sense to run a relay-server on the same machine as the main-server as the machine's bandwidth needs to be shared by two servers then.**

### Run a Icecast-Stack in a docker network

By using a [docker network](https://docs.docker.com/network/), the relay-server-container will be able to communicate with main-server-container by using container names instead of container addresses. Docker network resolves container addresses from their names. Thus, there is no need to explicitly specify ip-addresses or urls.

The environment variable `ENV_ICECAST_XML_RELAY_REMOTE_SERVER` of the relay container can therefore be set to the name of the main server container `my-icecast-server`, instead of using the container's address, ie. `192.168.1.13:8000`. `docker network` will resolve `my-icecast-server` to the corresponding container's address in the network.

In the following example, we just use a simple bridge-network that is suitable when running all containers on one physical machine. For real life deployment involving several physical hosts, a [overlay network](https://docs.docker.com/network/overlay/) could be used, or a [docker swarm](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Deployment/Run-Icecast-Stack#deploy-an-icecast-stack-on-real-hosts-with-docker-swarm). For now, the usage of an overlay network exceeds the scope of this guide and [will be added in the future](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Issues/New-Features#add-examples-in-documentation-about-running-an-icecast-stack-in-a-docker-overlay-network). 

``` bash
# first, create a docker network called 'icenet'

sudo docker network create icenet
```

``` bash
# start main icecast server in network 'icenet'

sudo docker run --rm -d --name my-icecast-server -p 8000:8000 \
                --network icenet \
                icecast-server-image
```

``` bash
# start icecast relay in network 'icenet'
# container port 8000 is mapped to localhost port 8001 (or any other port)
# as port 8000 is already in use by the main-server.

sudo docker run --rm -d --name my-icecast-relay -p 8001:8000 \
                --network icenet \
                --env ENV_ICECAST_XML_ENABLE_RELAY=1 \
                --env ENV_ICECAST_XML_LIMITS_SOURCES=1 \
                --env ENV_ICECAST_XML_RELAY_REMOTE_SERVER=my-icecast-server \
                --env ENV_ICECAST_XML_RELAY_REMOTE_PORT=8000 \
                --env ENV_ICECAST_XML_RELAY_REMOTE_MOUNT=/stream \
                --env ENV_ICECAST_XML_RELAY_LOCAL_MOUNT=/stream \
                icecast-server-image
```

```bash
# show running containers and filter by name containing 'icecast'
# output should at least show two running icecast containers. copy
# their <container-id> from output.
sudo docker ps --filter name=icecast

# now check the logs of the container with the id retrieved from previous command
sudo docker logs <container-id-main>
sudo docker logs <container-id-relay>

# try to reach the web-interface of the icecast containers
curl localhost:8000
curl localhost:8001
```

### Run Icecast-Stack without docker network

Without docker network, we need to tell the relay-server container where it should retrieve its stream from, by providing the address of the main-server container, in this case the ip-address of the local machine where both cnotainers are running on. Here we assume that the main server can be reached at `192.168.1.13:8000`.

Therefore, we set `ENV_ICECAST_XML_RELAY_REMOTE_SERVER=192.168.1.13`.

``` bash
# start main icecast server

sudo docker run --rm -d --name my-icecast-server -p 8000:8000 icecast-server-image
```

``` bash
# start icecast relay 

sudo docker run --rm -d --name my-icecast-relay -p 8001:8000 \
                --env ENV_ICECAST_XML_ENABLE_RELAY=1 \
                --env ENV_ICECAST_XML_LIMITS_SOURCES=1 \
                --env ENV_ICECAST_XML_RELAY_REMOTE_SERVER=192.168.1.13 \
                --env ENV_ICECAST_XML_RELAY_REMOTE_PORT=8000 \
                --env ENV_ICECAST_XML_RELAY_REMOTE_MOUNT=/stream \
                --env ENV_ICECAST_XML_RELAY_LOCAL_MOUNT=/stream \
                icecast-server-image
```

```bash
# show running containers and filter by name containing 'icecast'
# output should at least show two running icecast containers. copy
# their <container-id> from output.
sudo docker ps --filter name=icecast

# now check the logs of the container with the id retrieved from previous command
sudo docker logs <container-id-main>
sudo docker logs <container-id-relay>

# try to reach the web-interface of the icecast containers
curl localhost:8000
curl localhost:8001
```

### Run Icecast-Stack with docker-compose

Now let's start the icecast-stack with `docker compose` as defined in the repositorie's `docker-compose.yml` file.

The `docker-compose.yml` file in this repository sets all password to `changeme` instead of the default `hackme`. Take a look into the compose-file to see how the stack is configured.

`docker compose` will create a new network where all containers are run in. Container discovery is therefore enabled automatically through the docker-network. The name of the network is generated by `docker compose`. This is fine for us, as we do not need to know the network name.

Each service in `docker-compose.yml` has its own `container_name` set, in order to avoid that `docker compose` generates its own container names. Otherwise, we would need to set `ENV_ICECAST_XML_RELAY_REMOTE_SERVER` to the name generated by `docker compose`. 

The generated name is not fixed but might change depending on the directory in which one invokes `docker-compose up`. The local directory name is part of the generated name. As we use `container_name`, [the stack cannot be scaled](https://docs.docker.com/compose/compose-file/#container_name) (which we do not want anyways in this examples).

``` bash
# if not set explicitly via the -f <compose-file> flag, docker-compose will use
# the default 'docker-compose.yml' file residing in the current directory
sudo docker-compose up -d

# are the two services of the stack running?
sudo docker ps -f name=icecast

# are the web-interfaces reachable?
curl localhost:8000
curl localhost:8001
```

### Run Icecast-Stack in a Docker Stack/Swarm

Yet another option is to use a `docker stack` [in swarm mode](https://docs.docker.com/engine/reference/commandline/stack_deploy/) for icecast-stack deployment. When using `docker stack` a couple if things have to be considered:

- the stack runs all services defined in a `docker-compose` file. We cannot use the default `docker-compose.yml` file of this repository, as it uses compose version `2` syntax in order to be compatible with Rancher (that only supports version 2 syntax).
- the `docker stack` will assign a generated name to each container in the stack. Those names cannot be overwritten, as the `container_name` directive in the compose file will be ignored.

The container names are generated by `docker stack` as follows `<stack-name>_<compose-file-service-name>.<index>`:

- `<stack-name>` is set to `icecast-stack` when deploying the stack of this example
- `<compose-file-service-name>` is the name used in the compose-file for each service
- `<index>` is set to `1` in our example as we only use one main and one relay server (stack-scale is 1).

In order to run the icecast-stack via `docker stack`, we use the `docker-compose-docker-stack.yml` file in this repository. That compose-file can be found in the `share` subdirectory.

If you take a look into that file, you will see that the address in environment variable `ENV_ICECAST_XML_RELAY_REMOTE_SERVER` is still set to `icecast-server`.

This is ok as each container of a `docker stack` in swarm-mode does not need to know about the stack-name it runs in. The `docker-stack` will resolve the container service names as defined in the compose-file `services` sections.

The stack name is only necerssary when deploying different stacks that should interact with each other, ie. running the main icecast-server in one stack (with only one icecast-container) and the relay-servers in a another stack. In that case,  `ENV_ICECAST_XML_RELAY_REMOTE_SERVER` of each relay-container should be set to the full stack address of the main-icecast-server.

``` bash
# starts main icecast-server and icecast-relay, as defined in docker-compose-docker-stack.yml
# make sure that all environment variables are properly adjusted in docker-compose.yml

sudo docker stack deploy --orchestrator swarm -c share/docker-compose-docker-stack.yml icecast-stack

# are the services of the stack running?
sudo docker stack ps icecast-stack
```

In order to visit the containers web interfaces or start streaming, one thing is difference in a `docker stack` in swarm mode. Its not possible anymore to use `localhost` to acces the icecast-containers. Instead, the local machines ip-address must be used.  

```bash
# are the web-interfaces reachable?
curl 192.168.1.13:8000
curl 192.168.1.13:8001
```

## Deploy Icecast-Stack on remote machines 

All the [details can be found in the wiki](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Deployment/Run-Icecast-Stack):

- [start stack in Rancher (version 1.6)](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Deployment/Run-Icecast-Stack#deploy-an-icecast-stack-on-real-hosts-with-rancher-version-16)
- [start stack in Rancher (version 1.6) with Rancher Secrets](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Deployment/Run-Icecast-Stack#deploy-with-rancher-secrets)

- [start stack in a Docker Swarm](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Deployment/Run-Icecast-Stack#deploy-an-icecast-stack-on-real-hosts-with-docker-swarm)
- [start stack in a Docker Swarm with Docker Secrets](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Deployment/Run-Icecast-Stack#deploy-with-docker-secrets)

## Stream to local icecast server and visit status page 

Visit status page of local main and relay servers in your web-browser:

- `http://localhost:8000`
- `http://localhost:8001` (if relay is running) 

Login at status page as administrator (the same for main and relay server when using default config values):

- user    : `admin`  (the default admin user name)
- password: `hackme` (the default admin user password)

Stream to main-server with your [streaming software](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Handbook/Streaming):

- `http://localhost:8000`
- user    : `source` (the default streaming user name)
- password: `hackme` (the default streaming user password)

When using `docker-compose` to start a stack, another password is set for `admin` and `source` in the reposirory's `docker-compose.yml`:  

- password: `changeme`

Fetch stream from main or relay server, ie. with vlc or open the link in your browser:

- `http://localhost:8000/stream`
- `http://localhost:8001/stream` (if relay is running)

**Note: the relay-container might need some time to fetch the stream from the main-server. If relay-container's  streaming address is not reachable after container start, just reload the url for a couple of times or set your player to repeat the stream continously in order to retry connecting until the url is available.**

# [Streaming software](url)

Use [butt](https://danielnoethen.de/butt/), as it is free software and runs on Linux, MacOs and Windows.

More information can also be found [in the streaming section of the wiki](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Handbook/Streaming).

# Documentation

The full documentation for this container can be found in the [wiki](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/At-First-...).

# Based on

- [Official Alpine Linux base container](https://hub.docker.com/_/alpine) (version 3.12)
- [Icecast2](https://icecast.org/) (version 2.4.4)

# Release

All releases are [listed in the wiki](https://gitlab.com/collective.systems.community/csc-icecast/-/wikis/Releases).

# License

This project is under the GPL3 License. See the [LICENSE](https://gitlab.com/collective.systems.community/csc-icecast/-/blob/master/LICENSE) file for the full license text.

View license information for the software contained in this image at the [Alpine Linux Packages](https://pkgs.alpinelinux.org/packages) page.

As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, Python and Icecast, etc, along with any direct or indirect dependencies of the primary software being contained).

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.