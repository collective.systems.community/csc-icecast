#!/usr/bin/env bats

# Copyright (C) 2020 collective-systems
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.


setup() {
  rm -f ../src/icecast-generic.xml
  rm -f ../src/icecast.xml
  rm -f ../src/supervisord.conf

  rm -r -f ./tmp-secrets
}

teardown() {
  rm -f ../src/icecast-generic.xml
  rm -f ../src/icecast.xml
  rm -f ../src/supervisord.conf

  rm -r -f ./tmp-secrets
}

@test "invoke run-icecast.sh - show current envvars - supported" {
  source subst.env

  run ../src/run-icecast.sh show current

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -ne 0 ]

  local notSupported

  for i in "${!lines[@]}"; do
    notSupported=0
    [[ "${lines[$i]}" =~ (not supported) ]] && notSupported=1
    [[ ${notSupported} -eq 1 ]] && printf '# %s\n' "${lines[$i]}" >&3

    [ ${notSupported} -eq 0 ]
  done
}

@test "invoke run-icecast.sh - show current envvars - unsupported" {
  export ENV_ICECAST_XML_UNKNOWN=0
  export ENV_ICECAST_INTERNAL_UNKNOWN=0
  export ENV_ICECAST_UNKNOWN=0

  run ../src/run-icecast.sh show current

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 3 ]

  local notSupported
  for line in "${lines[@]}"; do
    notSupported=0
    [[ "${line}" =~ (not supported) ]] && notSupported=1
    [[ ${notSupported} -eq 0 ]] && printf '# %s\n' "${line}" >&3

    [ ${notSupported} -eq 1 ]
  done
}

@test "invoke run-icecast.sh - show defaults" {
  run ../src/run-icecast.sh show defaults

  # if default values in envvars-default.cfg are changed, this test should fail
  # and values used here must be readjusted

  # a list of default values as expected to be defined in envvars-default.cfg
  # NOTE ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH has no default value and is therefore
  #      not listed here.
  local -r expectedDefaults=(
    ENV_ICECAST_CONFIG_FILE_SOURCE="template"
    ENV_ICECAST_RUN_WITH_SUPERVISORD=0
    ENV_ICECAST_SECRETS_SHRED=0
    ENV_ICECAST_XML_ADMIN="icecast@localhost"
    ENV_ICECAST_XML_AUTHENTICATION_ADMIN_PWD="hackme"
    ENV_ICECAST_XML_AUTHENTICATION_RELAY_PWD="hackme"
    ENV_ICECAST_XML_AUTHENTICATION_SOURCE_PWD="hackme"
    ENV_ICECAST_XML_DIRECTORY_YP_URL="http://dir.xiph.org/cgi-bin/yp-cgi"
    ENV_ICECAST_XML_DIRECTORY_YP_URL_TIMEOUT="15"
    ENV_ICECAST_XML_ENABLE_DEFAULT_MOUNT=1
    ENV_ICECAST_XML_ENABLE_DIRECTORY=0
    ENV_ICECAST_XML_ENABLE_MOUNT_DUMP=0
    ENV_ICECAST_XML_ENABLE_MOUNT_STREAM_SETTINGS=0
    ENV_ICECAST_XML_ENABLE_RELAY=0
    ENV_ICECAST_XML_FILESERVE=1
    ENV_ICECAST_XML_HOSTNAME="127.0.0.1"
    ENV_ICECAST_XML_LIMITS_BURST_ON_CONNECT=0
    ENV_ICECAST_XML_LIMITS_BURST_SIZE_BYTES=65535
    ENV_ICECAST_XML_LIMITS_CLIENTS=100
    ENV_ICECAST_XML_LIMITS_QUEUE_SIZE_BYTES=524288
    ENV_ICECAST_XML_LIMITS_SOURCES=2
    ENV_ICECAST_XML_LISTEN_SOCKET_PORT=8000
    ENV_ICECAST_XML_LOCATION="'Second Earth'"
    ENV_ICECAST_XML_LOGGING_LOG_ARCHIVE=1
    ENV_ICECAST_XML_LOGGING_LOG_LEVEL=3
    ENV_ICECAST_XML_LOGGING_LOG_SIZE_BYTES=10000
    ENV_ICECAST_XML_MOUNT_DUMP_FILE_NAME="/dump/dump-%F-%H-%M-%S.mp3"
    ENV_ICECAST_XML_MOUNT_HIDDEN=0
    ENV_ICECAST_XML_MOUNT_PUBLIC=0
    ENV_ICECAST_XML_MOUNT_STREAM_DESCRIPTION="'whatever you say'"
    ENV_ICECAST_XML_MOUNT_STREAM_GENRE="experimental"
    ENV_ICECAST_XML_MOUNT_STREAM_NAME="'wild style world wide'"
    ENV_ICECAST_XML_MOUNT_STREAM_URL="http://dir.xiph.org/by_genre/experimental"
    ENV_ICECAST_XML_RELAY_LOCAL_MOUNT="/stream"
    ENV_ICECAST_XML_RELAY_LOCAL_ON_DEMAND=0
    ENV_ICECAST_XML_RELAY_LOCAL_SHOUTCAST_METADATA=0
    ENV_ICECAST_XML_RELAY_REMOTE_MOUNT="/stream"
    ENV_ICECAST_XML_RELAY_REMOTE_PORT=8000
    ENV_ICECAST_XML_RELAY_REMOTE_SERVER="127.0.0.1"
    ENV_ICECAST_XML_SECURITY_CHROOT=0
    ENV_ICECAST_INTERNAL_BIN_NAME="icecast"
    ENV_ICECAST_INTERNAL_BIN_PATH="/usr/bin"
    ENV_ICECAST_INTERNAL_NETCAT_BIN_NAME="nc"
    ENV_ICECAST_INTERNAL_NETCAT_BIN_PATH="/usr/bin"
    ENV_ICECAST_INTERNAL_SHRED_BIN_NAME="shred"
    ENV_ICECAST_INTERNAL_SHRED_BIN_PATH="/usr/bin"
    ENV_ICECAST_INTERNAL_SUPERVISORD_BIN_NAME="supervisord"
    ENV_ICECAST_INTERNAL_SUPERVISORD_BIN_PATH="/usr/bin"
    ENV_ICECAST_INTERNAL_SUPERVISORD_CONFIG_FILE_PATH="/etc"
  )

  [ ${status} -eq 0 ]

  local line
  local expected
  local match

  for i in ${!lines[@]}; do
    line="${lines[$i]}"
    expected="${expectedDefaults[$i]}"

    match=0
    [[ "${line}" == "${expected}" ]] && match=1
    [[ ${match} -eq 0 ]] && printf '# %s != %s\n' "${line}" "${expected}" >&3

    [ "${match}" -eq 1  ]
  done
}

@test "invoke run-icecast.sh - generated config - complete - with supervisord" {
  source subst.env
  ENV_ICECAST_RUN_WITH_SUPERVISORD=1

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
  [ -f ../src/icecast-generic.xml ]
  [ -f ../src/supervisord.conf ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines ../src/icecast-generic.xml expected-icecast-generic-complete.xml
  # no output if diff succeeds, just for the error case
  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines ../src/supervisord.conf expected-supervisord.conf
  # no output if diff succeeds, just for the error case
  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke run-icecast.sh - generated config - complete" {
  source subst.env

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
  [ -f ../src/icecast-generic.xml ]
  [ ! -f ../src/supervisord.conf ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines ../src/icecast-generic.xml expected-icecast-generic-complete.xml
  # no output if diff succeeds, just for the error case
  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke run-icecast.sh - generated config - no mount section" {
  source subst.env
  ENV_ICECAST_XML_ENABLE_DEFAULT_MOUNT=0

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
  [ -f ../src/icecast-generic.xml ]
  [ ! -f ../src/supervisord.conf ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines ../src/icecast-generic.xml expected-icecast-generic-no-default-mount.xml

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke run-icecast.sh - generated config - no mount dump" {
  source subst.env
  ENV_ICECAST_XML_ENABLE_MOUNT_DUMP=0

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
  [ -f ../src/icecast-generic.xml ]
  [ ! -f ../src/supervisord.conf ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines ../src/icecast-generic.xml expected-icecast-generic-no-dump.xml

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke run-icecast.sh - generated config - no mount stream settings" {
  source subst.env
  ENV_ICECAST_XML_ENABLE_MOUNT_STREAM_SETTINGS=0

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
  [ -f ../src/icecast-generic.xml ]
  [ ! -f ../src/supervisord.conf ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines ../src/icecast-generic.xml expected-icecast-generic-no-stream-settings.xml

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke run-icecast.sh - generated config - no relay" {
  source subst.env
  ENV_ICECAST_XML_ENABLE_RELAY=0

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
  [ -f ../src/icecast-generic.xml ]
  [ ! -f ../src/supervisord.conf ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines ../src/icecast-generic.xml expected-icecast-generic-no-relay.xml

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke run-icecast.sh - generated config - no directory" {
  source subst.env
  ENV_ICECAST_XML_ENABLE_DIRECTORY=0

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
  [ -f ../src/icecast-generic.xml ]
  [ ! -f ../src/supervisord.conf ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines ../src/icecast-generic.xml expected-icecast-generic-no-directory.xml

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke run-icecast.sh - generated config - complete - xml defaults" {

  # the internal envvars must be tweak here in order to
  # avoid the start of an locally installed icecast server
  # or an error message about missing icecast server
  export ENV_ICECAST_INTERNAL_BIN_NAME=echo
  export ENV_ICECAST_INTERNAL_BIN_PATH=/bin

  export ENV_ICECAST_INTERNAL_SUPERVISORD_BIN_NAME=echo
  export ENV_ICECAST_INTERNAL_SUPERVISORD_BIN_PATH=/bin

  export ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH='.'
  export ENV_ICECAST_INTERNAL_SUPERVISORD_CONFIG_FILE_PATH='.'

  run ../src/run-icecast.sh --dry-run

  [ ${status} -eq 0 ]
  [ -f ../src/icecast-generic.xml ]
  [ ! -f ../src/supervisord.conf ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines ../src/icecast-generic.xml expected-icecast-generic-complete-defaults.xml

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke run-icecast.sh - secrets - single secret - no deletion" {

  mkdir tmp-secrets
  cp expected-icecast-generic-complete.xml tmp-secrets

  source subst.env
  ENV_ICECAST_CONFIG_FILE_SOURCE="secrets"
  ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH="$(pwd)/tmp-secrets"

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
}

@test "invoke run-icecast.sh - secrets - single secret - overwrite and remove" {

  mkdir tmp-secrets
  cp expected-icecast-generic-complete.xml tmp-secrets

  source subst.env
  ENV_ICECAST_CONFIG_FILE_SOURCE="secrets"
  ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH="$(pwd)/tmp-secrets"
  # 1 indicates to overwrite AND remove the secrets file
  ENV_ICECAST_SECRETS_SHRED=1

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -ge 3 ]

  # the last three lines contain the arguments of the scripts command used
  # when using and deleting secrets. for the tests, the binary names are
  # substituted by the 'echo' command, as defined in 'subst.env' file

  # arguments of the icecast binary
  [ "${lines[-3]}" == "-c $(pwd)/tmp-secrets/expected-icecast-generic-complete.xml" ]
  # arguments of the netcat binary for probing the icecast port
  [ "${lines[-2]}" == "-w 2 localhost 8000" ]
  # arguments of the shred binary for deleting the icecast configuration
  [ "${lines[-1]}" == "-f -z -n 7 -u $(pwd)/tmp-secrets/expected-icecast-generic-complete.xml" ]

}

@test "invoke run-icecast.sh - secrets - single secret - overwrite only" {

  mkdir tmp-secrets
  cp expected-icecast-generic-complete.xml tmp-secrets

  source subst.env
  ENV_ICECAST_CONFIG_FILE_SOURCE="secrets"
  ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH="$(pwd)/tmp-secrets"
  # 2 indicates to overwrite secrets file only
  ENV_ICECAST_SECRETS_SHRED=2

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -ge 3 ]

  # the last three lines contain the arguments of the scripts command used
  # when using and deleting secrets. for the tests, the binary names are
  # substituted by the 'echo' command, as defined in 'subst.env' file

  # arguments of the icecast binary
  [ "${lines[-3]}" == "-c $(pwd)/tmp-secrets/expected-icecast-generic-complete.xml" ]
  # arguments of the netcat binary for probing the icecast port
  [ "${lines[-2]}" == "-w 2 localhost 8000" ]
  # arguments of the shred binary for deleting the icecast configuration, we expect al -u to
  [ "${lines[-1]}" == "-f -z -n 7 $(pwd)/tmp-secrets/expected-icecast-generic-complete.xml" ]
}

@test "invoke run-icecast.sh - secrets - multiple secrets" {

  # lets put two files into the fake secrets mount point
  # to cause an error
  mkdir tmp-secrets
  cp expected-icecast-generic-complete.xml tmp-secrets
  cp expected-icecast-generic-complete-defaults.xml tmp-secrets

  source subst.env
  ENV_ICECAST_CONFIG_FILE_SOURCE="secrets"
  ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH="$(pwd)/tmp-secrets"

  run ../src/run-icecast.sh

  [ ${status} -eq 1 ]
  [ "${output}" == "error: expected one secrets-file at [$(pwd)/tmp-secrets] but got [expected-icecast-generic-complete-defaults.xml expected-icecast-generic-complete.xml] ... stopping" ]
}

@test "invoke run-icecast.sh - secrets - no secret" {

  mkdir tmp-secrets

  source subst.env
  ENV_ICECAST_CONFIG_FILE_SOURCE="secrets"
  ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH="$(pwd)/tmp-secrets"

  run ../src/run-icecast.sh

  [ ${status} -eq 1 ]
  [ "${output}" == "error: expected one secrets-file at [$(pwd)/tmp-secrets] but got [] ... stopping" ]
}

@test "invoke run-icecast.sh - secrets - invalid secret" {

  mkdir tmp-secrets
  echo "this is just a text file" >tmp-secrets/invalid.xml

  source subst.env
  ENV_ICECAST_CONFIG_FILE_SOURCE="secrets"
  ENV_ICECAST_INTERNAL_CONFIG_FILE_PATH="$(pwd)/tmp-secrets"

  run ../src/run-icecast.sh

  [ ${status} -eq 1 ]
  [ "${output}" == "error: provided file [$(pwd)/tmp-secrets/invalid.xml] is no valid xml file ... stopping" ]
}


@test "invoke run-icecast.sh - default config" {
  # just reuse one of the test xml files
  cp expected-icecast-generic-complete.xml ../src/icecast.xml

  source subst.env
  ENV_ICECAST_CONFIG_FILE_SOURCE="default"

  run ../src/run-icecast.sh

  [ ${status} -eq 0 ]
  [ ! -f ../src/supervisord.conf ]
}

@test "invoke run-icecast.sh - default config - invalid" {
  # xml file only contains text to cause an error
  echo "this is just a text file" >../src/icecast.xml

  source subst.env
  ENV_ICECAST_CONFIG_FILE_SOURCE="default"

  run ../src/run-icecast.sh

  [ ${status} -eq 1 ]
  [ "${output}" == "error: provided file [./icecast.xml] is no valid xml file ... stopping" ]

  [ ! -f ../src/supervisord.conf ]
}
