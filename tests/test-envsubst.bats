#!/usr/bin/env bats

# Copyright (C) 2020 collective-systems
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.


declare -g -r ENV_TEST_LINE_A="line a"
declare -g -r ENV_TEST_LINE_B="line b"
declare -g -r ENV_TEST_LINE_C="line c"

@test "invoke envsubst.sh" {
  run ../tools/envsubst.sh "ENV_TEST" <<-EOF
a = ${ENV_TEST_LINE_A}
b = ${ENV_TEST_LINE_B}
c = ${ENV_TEST_LINE_C}
EOF

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "a = line a" ]
  [ "${lines[1]}" == "b = line b" ]
  [ "${lines[2]}" == "c = line c" ]
}
