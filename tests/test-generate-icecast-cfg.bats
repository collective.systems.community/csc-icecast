#!/usr/bin/env bats

# Copyright (C) 2020 collective-systems
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.


@test "invoke generate-icecast-cfg.sh - complete" {
  source subst.env

  run ../tools/generate-icecast-cfg.sh

  [ ${status} -eq 0 ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines - expected-icecast-generic-complete.xml < <(printf '%s\n' "${lines[@]}")

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke generate-icecast-cfg.sh - no mount section" {
  source subst.env
  ENV_ICECAST_XML_ENABLE_DEFAULT_MOUNT=0

  run ../tools/generate-icecast-cfg.sh

  [ ${status} -eq 0 ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines - expected-icecast-generic-no-default-mount.xml < <(printf '%s\n' "${lines[@]}")

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke generate-icecast-cfg.sh - no mount dump" {
  source subst.env
  ENV_ICECAST_XML_ENABLE_MOUNT_DUMP=0

  run ../tools/generate-icecast-cfg.sh

  [ ${status} -eq 0 ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines - expected-icecast-generic-no-dump.xml < <(printf '%s\n' "${lines[@]}")

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke generate-icecast-cfg.sh - no stream settings" {
  source subst.env
  ENV_ICECAST_XML_ENABLE_MOUNT_STREAM_SETTINGS=0

  run ../tools/generate-icecast-cfg.sh

  [ ${status} -eq 0 ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines - expected-icecast-generic-no-stream-settings.xml < <(printf '%s\n' "${lines[@]}")

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke generate-icecast-cfg.sh - no relay" {
  source subst.env
  ENV_ICECAST_XML_ENABLE_RELAY=0

  run ../tools/generate-icecast-cfg.sh

  [ ${status} -eq 0 ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines - expected-icecast-generic-no-relay.xml < <(printf '%s\n' "${lines[@]}")

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke generate-icecast-cfg.sh - no directory" {
  source subst.env
  ENV_ICECAST_XML_ENABLE_DIRECTORY=0

  run ../tools/generate-icecast-cfg.sh

  [ ${status} -eq 0 ]

  run diff --ignore-tab-expansion --ignore-space-change --ignore-blank-lines - expected-icecast-generic-no-directory.xml < <(printf '%s\n' "${lines[@]}")

  [[ ${status} -ne 0 ]] && printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}
